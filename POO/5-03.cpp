#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <string>
using namespace std;
/*Se tiene un archivo “inscriptos.txt” con una lista de nombres de alumnos inscriptos
al cursado de Fundamentos de Programación. Se desea distribuir los estudiantes
en comisiones de práctica de no más de 30 alumnos. Escriba un programa que
determine cuantas comisiones deberían formarse de acuerdo a la cantidad de
inscriptos y reparta los alumnos en comisiones de igual tamaño, guardando la lista
de alumnos de cada comisión en archivo de texto “comision1.txt”, “comision2.txt”, ...
“comisionN.txt”.
Ayuda: puede utilizar la clase stringstream como auxiliar para concatenar en un
string texto y números para formar los nombres de los archivos. */

int main(int argc, char const *argv[])
{

	ifstream inscriptos_i("inscriptos.txt");
	vector<string> alumnos;
	string aux;

	int n=0;
	const int cupo = 5;
	int agregados  = 0;

	while (getline(inscriptos_i, aux))
	{
		++n;
		alumnos.push_back(aux);
	}

		int n_comisiones = n/cupo;
		ofstream* comisiones = new ofstream[n_comisiones];
		
		for (n = 0; n < n_comisiones; ++n)
		{
			comisiones[n].open("comision" + to_string(n+1));

				for (int i = agregados; i < cupo+agregados; ++i)
				{
					comisiones[n] << alumnos[i] << endl;

				}
						agregados += cupo;
		}

delete[] comisiones;
return 0;
}