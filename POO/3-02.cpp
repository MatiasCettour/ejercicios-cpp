#include <iostream>
using namespace std;

/*Utilice las clases ​ Alumno y ​ Docente del ejercicio anterior para crear una clase
Curso que modele el cursado de una materia. Cada curso tiene un nombre, un
profesor a cargo y un número máximo de 50 alumnos. Implemente un método
AgregarAlumno(...) que permita agregar un alumno al curso y otro método
MejorPromedio(...) que devuelva el alumno con mejor promedio. Proponga los
constructores y métodos extra que considere necesarios.*/

class Persona
{
public:
	Persona(string apellido, string nombre): m_apellido(apellido),m_nombre(nombre){};

	void set_apellido(string apellido)			   { m_apellido = apellido; }
	void set_nombre(string nombre)				   { m_nombre = nombre; }
	void set_dni(int dni)						   { m_dni = dni; }
	void set_fecha_nacimiento(int fecha_nacimiento){ m_fecha_nacimiento = fecha_nacimiento; }
	void set_estado_civil(string estado_civil)	   { m_estado_civil = m_estado_civil; }

	int get_edad(int fecha);

	~Persona(){}

protected:

	string m_apellido;
	string m_nombre;
	int m_dni;
	int m_fecha_nacimiento;
	string m_estado_civil;

};

class Alumno : public Persona
{
public:

	Alumno(string apellido, string nombre): Persona(apellido,nombre){}
	Alumno(): Persona("null","null"){}

	void set_materias_aprobadas(int cantindad) { m_materias_aprobadas = cantindad; }
	void set_promedio		   (float promedio){           m_promedio = promedio; }
	float get_promedio(){ return m_promedio; }

	float get_merito_academico(){ return m_promedio*m_materias_aprobadas; }

protected:
	float m_promedio = 0;
	int m_materias_aprobadas = 0; 
};

class Docente : public Persona
{
public:

	Docente(string apellido, string nombre): Persona(apellido,nombre){}

	void set_fecha_ingreso(int fecha){ m_fecha_ingreso = fecha; }

	int get_antiguedad(int fecha);

	~Docente(){}

private: int m_fecha_ingreso;
};

class Curso
{
public:
	Curso(string nombre, Docente docente) : m_nombre(nombre), m_docente_a_cargo(docente){}

	void agregar_alumno(Alumno alumno);
	float get_mejor_promedio();

	~Curso(){ delete[] m_alumnos; }

private:
	string m_nombre;
	Docente m_docente_a_cargo;
	Alumno* m_alumnos = nullptr;
	int m_cantidad_alumnos = 0;
};


int Persona::get_edad(int fecha)
{
	int year_actual, mes_actual, dia_actual;
	int year, mes, dia;

		year_actual = fecha%10000;
		mes_actual  = (fecha/10000)%100;
		dia_actual  = fecha/1000000;

		year = m_fecha_nacimiento%10000;
		mes  = (m_fecha_nacimiento/10000)%100;
		dia  = m_fecha_nacimiento/1000000;

			if (mes_actual >= mes && dia_actual >= dia) return year_actual-year;

			else return year_actual-year-1;
}

int Docente::get_antiguedad(int fecha)
{
	int year_actual, mes_actual, dia_actual;
	int year, mes, dia;

		year_actual = fecha%10000;
		mes_actual  = (fecha/10000)%100;
		dia_actual  = fecha/1000000;

		year = m_fecha_ingreso%10000;
		mes  = (m_fecha_ingreso/10000)%100;
		dia  = m_fecha_ingreso/1000000;

			if (mes_actual >= mes && dia_actual >= dia) return year_actual-year;

			else return year_actual-year-1;
}

void Curso::agregar_alumno(Alumno alumno)
{
	if (m_cantidad_alumnos < 50)
	{
		if (m_cantidad_alumnos == 0) 
		{
			Alumno* new_alumnos = new Alumno[1];
			m_alumnos 	 = new_alumnos; 
			m_alumnos[0] = alumno;
			++m_cantidad_alumnos;
		}
		else
		{
			Alumno* new_alumnos = new Alumno[m_cantidad_alumnos+1];
			for (int i = 0; i < m_cantidad_alumnos; ++i)
			{
				new_alumnos[i] = m_alumnos[i];
			}
					new_alumnos[m_cantidad_alumnos] = alumno;
					m_cantidad_alumnos++;

						delete[] m_alumnos;

							m_alumnos = new_alumnos;
		}
	}
	else cout << "ERROR!"<<endl;
}

float Curso::get_mejor_promedio()
{
	float mejor_promedio = 0;

	for (int i = 0; i < m_cantidad_alumnos; ++i)
	{
		if (m_alumnos[i].get_promedio() > mejor_promedio) mejor_promedio = m_alumnos[i].get_promedio();
	}
			return mejor_promedio;
}
//________________________________________________________________________________________________________________________________________________________________
int main(int argc, char const *argv[])
{
	Alumno juan("Rafael", "Juan");
	Alumno matias("Politi", "Matias");
	Docente edith("Ramirez", "Edith");

	juan.set_dni(41248246);
	juan.set_fecha_nacimiento(781994);
	juan.set_promedio(7.5);
	juan.set_materias_aprobadas(30);

	matias.set_dni(41248246);
	matias.set_fecha_nacimiento(781994);
	matias.set_promedio(8.5);
	matias.set_materias_aprobadas(30);

	edith.set_dni(41268246);
	edith.set_fecha_nacimiento(341980);
	edith.set_fecha_ingreso(1052010);

		Curso curso("POO",edith);

		curso.agregar_alumno(juan);
		curso.agregar_alumno(matias);

		cout << "Mejor promedio POO: " << curso.get_mejor_promedio() <<"\n\n";

	return 0;
}