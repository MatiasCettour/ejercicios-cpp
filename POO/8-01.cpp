#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;
/*Escriba un programa que permita al usuario ingresar 15 valores por teclado, los
almacene en un vector y luego:
a. Muestre el vector generado utilizando 3 mecanismos de iteración diferentes.
	1. utilizando el operador []
	2. utilizando iteradores
	3. utilizando el for basado en rangos
	4. ¿en qué caso es conveniente utilizar cada mecanismo?
b. Calcule y muestre
	1. los valores de los elementos máximo y mínimo
	2. la suma de todos los elementos del arreglo
	3. el promedio y la mediana de los elementos del arreglo
c. Permita al usuario ingresar un valor, e informe si se encuentra en el vector,
y en caso afirmativo, en qué posición.*/

int main(int argc, char const *argv[])
{
	vector<int> v(15);
	for (int& x : v)
	{
		//cin >> x;
		x = rand()%10;
	}

	cout << "Operador []: "; for (size_t i = 0; i < v.size(); ++i) cout << v[i] << " "; //util para vectores

	cout << endl << "Iteradores: "; for (vector<int>::iterator it = v.begin(); it != v.end(); ++it) //necesario para iteradores
									{
										cout << *it << " ";
									}
	cout << endl << "For basado en rangos: "; for (const int& x : v) cout << x << " "; //util para acceder a todos los elementos, una solo vez y si no se necesita el indice

	vector<int>::iterator it_max = max_element(v.begin(), v.end());
	vector<int>::iterator it_min = min_element(v.begin(), v.end());
	cout << endl << "Max element: " << *it_max << endl << "Min element: " << *it_min << endl;

	int suma = accumulate(v.begin(), v.end(), 0);
	cout << "Suma: " << suma << endl;

	cout << "Promedio: " << suma/v.size() << endl;

	sort(v.begin(), v.end());
	auto it_med = next(v.begin(), v.size()/2);
	cout << "Mediana: " << *it_med << endl;

	cout << "Ingrese valor a buscar > "; int valor; cin >> valor;
	auto it_valor = find(v.begin(), v.end(), valor);
		if (it_valor == v.end()) cout << "Valor no encontrado" << endl;
		else cout << "El valor esta en la posicion " << distance(v.begin(), it_valor) << endl;

	return 0;
}