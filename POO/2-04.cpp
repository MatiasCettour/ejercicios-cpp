#include <iostream>
#include <vector>
using namespace std;

/*Implemente una clase ​ VectorDinamico que posea como atributo un puntero a
entero que apunte a la memoria donde se almacenan los datos. Dicha clase debe
poseer:
a. Un constructor que reciba el tamaño inicial del vector, reserve la memoria
necesaria e inicialice los valores del vector de manera aleatoria.
b. Un destructor que se encargue de liberar la memoria reservada.
c. Un método ​ Duplicar(...) que duplique la cantidad de memoria reservada
manteniendo los datos que ya estaban en el vector e inicializando al azar
los nuevos valores.
d. Un método ​ VerElemento(...) que reciba el número de elemento y devuelva
su valor.
e. Cree un programa cliente que muestre la utilización de todas las funciones
implementadas.
f. Explique: ¿Es necesario implementar un constructor de copia?¿Por qué?*/

class VectorDinamico
{
public:

	explicit VectorDinamico(int size);

	void duplicar();
	int get_elemento(int pos);
	int size();
	
	~VectorDinamico(){ delete[] m_vector; }

private:

	int* m_vector = nullptr;
	int m_size;
};

VectorDinamico::VectorDinamico(int size)
{
	m_vector = new int[size];
	m_size   = size;

	for (int i = 0; i < size; ++i) m_vector[i] = rand() % 1000;
}

void VectorDinamico::duplicar()
{
	int* p = new int[m_size*2];

		for (int i = 0; i < m_size; ++i) p[i] = m_vector[i];

		for (int j = m_size*2; j < m_size*2; ++j) p[j] = rand() % 1000;

			m_size *= 2;

delete[] m_vector;

	m_vector = p;
}

int VectorDinamico::get_elemento(int pos) { return *(m_vector + pos); }

int VectorDinamico::size() { return m_size; }


int main(int argc, char const *argv[])
{
	VectorDinamico v {10};

	for (int i = 0; i < v.size(); ++i) cout << v.get_elemento(i) << " ";
cout << "\n";

	v.duplicar();

for (int i = 0; i < v.size(); ++i) cout << v.get_elemento(i) << " ";
cout << "\n";

		return 0;
}