#include <iostream>
using namespace std;

/*Diseñe una clase ​ Persona que contenga los siguientes atributos: apellido y
nombre, DNI, fecha de nacimiento y estado civil. La clase debe poseer, además, un
método ​ Edad(...) que calcule la edad actual de la persona en base a la fecha de
nacimiento y la fecha actual (que recibe como argumento).

Implemente una clase ​ Alumno para contener la siguiente información de un
alumno: apellido y nombre, DNI, año de nacimiento, estado civil, promedio y
cantidad de materias aprobadas. La clase debe poseer, además, un método
MeritoAcademico() que devuelve el mérito académico del alumno (éste se calcula
como el producto entre el promedio y la cantidad de materias aprobadas).

Cree, también, una clase ​ Docente para modelar un docente a partir de la siguiente
información: apellido y nombre, DNI, año de nacimiento, estado civil y fecha de
ingreso. La clase debe poseer, además, un método ​ Antiguedad() que calcule la
antigüedad del docente en base a su fecha de ingreso y la fecha actual.

Proponga una jerarquía de clases adecuada para evitar repetir atributos.
Implemente constructores y métodos extra que considere adecuados. Codifique un
programa cliente que cree instancias de ​ Alumno​ y ​ Docente​ y utilice sus funciones.

Responda:
● ¿Puede crearse un objeto de tipo persona? ¿Para qué sirve esto?
● ¿Existe alguna clase abstracta en la jerarquía?*/

class Persona
{
public:
	Persona(string apellido, string nombre): m_apellido(apellido),m_nombre(nombre){};

	void set_apellido(string apellido)			   { m_apellido = apellido; }
	void set_nombre(string nombre)				   { m_nombre = nombre; }
	void set_dni(int dni)						   { m_dni = dni; }
	void set_fecha_nacimiento(int fecha_nacimiento){ m_fecha_nacimiento = fecha_nacimiento; }
	void set_estado_civil(string estado_civil)	   { m_estado_civil = m_estado_civil; }

	int get_edad(int fecha);

	~Persona(){}

protected:

	string m_apellido;
	string m_nombre;
	int m_dni;
	int m_fecha_nacimiento;
	string m_estado_civil;

};

class Alumno : public Persona
{
public:

	Alumno(string apellido, string nombre): Persona(apellido,nombre){}

	void set_materias_aprobadas(int cantindad) { m_materias_aprobadas = cantindad; }
	void set_promedio		   (float promedio){           m_promedio = promedio; }

	float get_merito_academico(){ return m_promedio*m_materias_aprobadas; }

private:
	float m_promedio;
	int m_materias_aprobadas; 
};
class Docente : public Persona
{
public:
	Docente(string apellido, string nombre): Persona(apellido,nombre){}

	void set_fecha_ingreso(int fecha){ m_fecha_ingreso = fecha; }

	int get_antiguedad(int fecha);

	~Docente(){}

private: int m_fecha_ingreso;
};

int Persona::get_edad(int fecha)
{
	int year_actual, mes_actual, dia_actual;
	int year, mes, dia;

		year_actual = fecha%10000;
		mes_actual  = (fecha/10000)%100;
		dia_actual  = fecha/1000000;

		year = m_fecha_nacimiento%10000;
		mes  = (m_fecha_nacimiento/10000)%100;
		dia  = m_fecha_nacimiento/1000000;

			if (mes_actual >= mes && dia_actual >= dia) return year_actual-year;

			else return year_actual-year-1;
}

int Docente::get_antiguedad(int fecha)
{
	int year_actual, mes_actual, dia_actual;
	int year, mes, dia;

		year_actual = fecha%10000;
		mes_actual  = (fecha/10000)%100;
		dia_actual  = fecha/1000000;

		year = m_fecha_ingreso%10000;
		mes  = (m_fecha_ingreso/10000)%100;
		dia  = m_fecha_ingreso/1000000;

			if (mes_actual >= mes && dia_actual >= dia) return year_actual-year;

			else return year_actual-year-1;
}

//________________________________________________________________________________________________________________________________________________________________
int main(int argc, char const *argv[])
{
	Persona matias("Cettour", "Matias");
	Alumno juan("Rafael", "Juan");
	Docente edith("Gonsalez", "Edith");

		matias.set_dni(41268246);
		matias.set_estado_civil("Soltero");
		matias.set_fecha_nacimiento(7101998);

		juan.set_dni(41248246);
		juan.set_estado_civil("Soltero");
		juan.set_fecha_nacimiento(781994);
		juan.set_promedio(7.5);
		juan.set_materias_aprobadas(30);

		edith.set_dni(41268246);
		edith.set_estado_civil("Soltero");
		edith.set_fecha_nacimiento(341980);
		edith.set_fecha_ingreso(1052010);

			cout <<"Edad matias: "<<matias.get_edad(1112020)<<"\n\n";

			cout << "Merito academico Juan: "<<juan.get_merito_academico()<<"\n\n";

			cout << "Fecha antiguedad Edith: "<<edith.get_antiguedad(1112020)<<"\n\n";


	return 0;
}