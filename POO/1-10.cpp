#include <iostream>
using namespace std;

/*Generar aleatoriamente una matriz de números reales de doble precisión de 10
filas por 6 columnas mediante un puntero a un arreglo. Usando notación de
punteros determine e informe: a) el promedio de la fila que el usuario ingrese como
dato; b) la suma de cada columna.
Analice: ¿sería posible, manteniendo la estructura de puntero a arreglo, que el
usuario ingresara el tamaño de la matriz (filas x columnas)?*/

double m[10][6];

double (*p)[6] = m;

//____________________________________________________________________________________________________________________________
int main(int argc, char const *argv[])
{

for(int i = 0; i < 10; ++i)
{
cout << endl;

	for(int j = 0; j < 6; ++j)
	{
		*(*(p+i)+j) = rand() % + 50;
		cout << *(*(p+i)+j) << " "; //m[i][j]
	} 
} 

cout << "\n\n" << "Ingrese  n° fila > "; int fila; cin >> fila;

int promedio, suma = 0;

	for(int j = 0; j < 6; ++j)
	{
		suma += *(*(p+fila)+j);
	} 

promedio = suma/6;

cout << "\n\n" << "Promedio: " << promedio << endl;


return 0;
}
//____________________________________________________________________________________________________________________________

