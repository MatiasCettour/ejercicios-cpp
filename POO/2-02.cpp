#include <iostream>
#include <tuple>
#include <math.h>
using namespace std;
/* Proponga una clase ​ EcuacionCuadratica para modelar ecuaciones cuadráticas de
la forma ​ ax 2 ​ ​ +bx+c=0 . ​ La clase debe incluir:
a. Un constructor que reciba los valores de los coeficientes ​ a, b y c . ​
b. Un método ​ TieneRaicesReales(...) que devuelva verdadero si las raíces de
la ecuación son reales.
c. Dos métodos ​ VerRaiz1(...) y ​ VerRaiz2(...) que permitan obtener las raíces
reales (en caso de que lo sean).
d. Dos métodos ​ VerParteReal(...) y ​ VerParteImag(...) que permitan obtener las
partes real e imaginaria de las raíces complejas (en caso de que lo sean).
e. Cree un programa cliente que utilice un objeto de la clase
EcuaciónCuadratica ​ para determinar las raíces de una ecuación
cuadrática cuyos coeficientes sean ingresados por el usuario, y las muestre
en el formato que corresponda (según sean reales o complejas). */

struct complex
{
	float real = 0;
	float imag = 0;
};

class EcuacionCuadratica
{
public:

	EcuacionCuadratica(float a, float b, float c) : m_a(a), m_b(b), m_c(c),
													m_dis( (m_b*m_b) - (4*m_a*m_c) ) {}
	
	complex get_raiz1();
	complex get_raiz2();
	bool tiene_raices_reales();

	~EcuacionCuadratica(){};

private:

	float m_a, m_b, m_c;
	float m_dis;
	complex m_r1, m_r2;

	complex calcular_raiz_real1();
	complex calcular_raiz_real2();
	complex calcular_raiz_imaginaria1();
	complex calcular_raiz_imaginaria2();
};

complex EcuacionCuadratica::get_raiz1()
{
	if (tiene_raices_reales()) calcular_raiz_real1();

	else calcular_raiz_imaginaria1();

return m_r1;
}

complex EcuacionCuadratica::get_raiz2()
{

	if (tiene_raices_reales()) calcular_raiz_real2();

	else calcular_raiz_imaginaria2();

return m_r2;
}

bool EcuacionCuadratica::tiene_raices_reales()
{
	if (m_dis >= 0) return true;

	else return false;
}

complex EcuacionCuadratica::calcular_raiz_real1(){

	m_r1.real = (-m_b + sqrt(m_dis)) / (2.*m_a);

return m_r1;
}

complex EcuacionCuadratica::calcular_raiz_real2(){

	m_r2.real = (-m_b - sqrt(m_dis)) / (2.*m_a);

return m_r2;
}

complex EcuacionCuadratica::calcular_raiz_imaginaria1(){

	m_r1.real = -m_b/(2*m_a);
	m_r1.imag = sqrt(-m_dis)/(2*m_a);

return m_r1;
}
complex EcuacionCuadratica::calcular_raiz_imaginaria2(){

	m_r2.real = -m_b/(2*m_a);
	m_r2.imag = -sqrt(-m_dis)/(2*m_a);

return m_r2;
}
//----------------------------------------------------------------------------------------------------------------

void print_raices(EcuacionCuadratica* ec)
{
	if(ec->tiene_raices_reales())
	{
		cout << "x1: " << ec->get_raiz1().real << " | x2:" << ec->get_raiz2().real;
	}
	else cout << "x1: " << ec->get_raiz1().real <<" + "<< ec->get_raiz1().imag <<"i"
			<< " | x2:" << ec->get_raiz2().real <<" + "<< ec->get_raiz2().imag <<"i";

cout << endl;
}

int main()
{
//cout << "Ingrese coeficientes > ";
//float a,b,c; cin>>a;cin>>b;cin>>c;

EcuacionCuadratica ec1(1,-5,6), ec2(2,-2,5);

print_raices(&ec1);
print_raices(&ec2);

cout << endl;
return 0;
}
