#include "iostream"
using namespace std;
/*Implemente sobrecargas para los operadores >> Y << para leer un objeto de tipo
Racional desde la consola (mediante cin) y mostrarlo en pantalla (mediante cout).
La lectura se debe realizar leyendo el numerador y denominador por separado (es
decir, separados por un espacio o salto de línea). El operador << debe mostrar el
numerador y el denominador separados por el carácter '/'. Analice: la sobrecarga
de este operador, ¿debe realizarse dentro o fuera de una clase? */

class Racional
{
public:
	Racional(double n, double d) : num(n), den(d){}

	double get_numerador()   const { return num; }
	double get_denominador() const { return den; }

	void set_numerador  (double valor){ num = valor; }
	void set_denominador(double valor){ den = valor; }

	Racional& operator++()
	{
		num = num + den;
	return *this;
	}

	Racional operator++(int)
	{
		Racional aux = *this;
		num = num + den;
	return aux;
	}

	Racional operator+ (const Racional& r2)
	{
		Racional suma((this->get_numerador()*r2.get_denominador()) + (r2.get_numerador()*this->get_denominador()),
								this->get_denominador()*r2.get_denominador()  );

	return suma;
	}

	Racional operator* (const Racional& r2)
	{
		Racional multiplicacion(this->get_numerador()*r2.get_numerador(),
								this->get_denominador()*r2.get_denominador());

	return multiplicacion;
	}

	bool operator<  (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) < (r2.get_numerador() / r2.get_denominador()); }
	bool operator>  (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) > (r2.get_numerador() / r2.get_denominador()); }
	bool operator<= (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) <= (r2.get_numerador() / r2.get_denominador()); }
	bool operator>= (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) >= (r2.get_numerador() / r2.get_denominador()); }
	bool operator== (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) == (r2.get_numerador() / r2.get_denominador()); }
	bool operator!= (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) != (r2.get_numerador() / r2.get_denominador()); }
	
private: double num, den;
};

ostream &operator<< (ostream &o, Racional r)
{
	o << r.get_numerador() << "/" << r.get_denominador() << endl;
return o;
}

istream &operator>> (istream& i, Racional& r)
{
	double n; i >> n; r.set_numerador(n);
	double d; i >> d; r.set_denominador(d);
return i;
}

int main(int argc, char const *argv[])
{
	Racional a(3, 5), b(1, 3), c(1, 3);
	cout << (b < a) << endl;
	cout << (b > a) << endl;
	cout << (b <= a) << endl;
	cout << (b >= a) << endl;
	cout << (b == a) << endl;
	cout << (b != a) << endl;
	c = a+b;
	cout << c;
	c = a*b;
	c = a+b+c;
	cout << c.get_numerador() << " " << c.get_denominador() << endl;
	c = a+b+c;
	b = c++;
	a = ++c;
	cout << a.get_numerador() << " " << a.get_denominador() << endl;
	cout << b.get_numerador() << " " << b.get_denominador() << endl;
	return 0;
}