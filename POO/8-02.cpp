#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;
/*Escriba un programa que defina un vector dinámico de 30 enteros aleatorios
menores que 25. Luego:
a. Ordene en forma descendente los elementos ubicados entre las posiciones
10 y 20 inclusive, y muestre el vector.
b. Inserte en un nuevo vector los elementos que fueron ordenados en el
apartado anterior, y quítelos del vector original.
c. Permita ingresar un valor por teclado, y muestre cuántas veces aparece
dicho valor en el vector.
d. Elimine todas las ocurrencias del valor ingresado en el punto c, utilizando la
función remove. Responda: ¿Pueden las funciones globales de la STL
eliminar realmente los elementos (liberando la memoria de un contenedor)?*/

int main(int argc, char const *argv[])
{

	vector<int> v(30, 0);
	for (int& x : v)
	{
		x = rand()%25;
	} 
	for (const int& x : v) cout << x << " "; cout << endl;

		sort(v.begin()+10, v.begin()+20);

		reverse(v.begin()+10, v.begin()+20);

	for (const int& x : v) cout << x << " "; cout << endl;

			vector<int> v2(10);
			copy(v.begin()+10, v.begin()+20, v2.begin());

			for (size_t i = 10; i < 20; ++i)
			{
				v[i] = v[i+10];
			}
			v.resize(20);
			for (const int& x : v) cout << x << " "; cout << endl;
			for (const int& x : v2) cout << x << " "; cout << endl;

					cout << "Ingrese valor a buscar > "; int valor; cin >> valor;
					int n_veces = count(v.begin(), v.end(), valor);
					cout << valor << " encontrado " << n_veces << " veces" << endl;

						auto it_remove = remove(v.begin(), v.end(), valor); //d)no borra valores, los agrupa al final
						v.erase(it_remove, v.end());
						for (const int& x : v) cout << x << " "; cout << endl;

return 0;
}