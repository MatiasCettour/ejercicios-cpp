#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;
/*Programe una clase templatizada llamada ​ VectorDinamico (similar a la de la guía
2). La clase debe poseer:
a. Un constructor que reciba el tamaño inicial del vector, y reserve la memoria
necesaria.
b. Un destructor que se encargue de liberar la memoria reservada.
c. Una sobrecarga del operador[] que reciba el número de elemento, devuelva
su valor, y permita modificarlo.
d. Modifique o sobrecargue el constructor para que permita generar valores
aleatorios con los cuales inicializar las posiciones del arreglo que reserva.
e. Utilice la clase desde un programa cliente creando vectores aleatorios con
diversos tipos de datos (int,double,string, etc).*/

template<typename T>
class VectorDinamico
{
public:
	VectorDinamico(int _size);
	VectorDinamico(int _size, char);
	VectorDinamico(int _size, string);

	int get_size();

	T& operator[](int i);

	~VectorDinamico();

private:
	T* m_ptr = nullptr;
	int m_size;
};

template<typename T>
VectorDinamico<T>::VectorDinamico(int size) : m_size(size)
{
	m_ptr = new T[size];
}

template<typename T>
VectorDinamico<T>::VectorDinamico(int size, char) : m_size(size)
{
	m_ptr = new T[size];

	for (size_t i = 0; i < size; ++i)
	{
		m_ptr[i] = rand() % 1000;
	}
}

template<typename T>
VectorDinamico<T>::VectorDinamico(int size, string) : m_size(size)
{
	m_ptr = new T[size];

	for (size_t i = 0; i < size; ++i)
	{
		m_ptr[i] = to_string(rand() % 1000);
	}
}

template<typename T>
VectorDinamico<T>::~VectorDinamico()
{
	delete[] m_ptr;
}

template<typename T>
int VectorDinamico<T>::get_size()
{
	return m_size;
}

template<typename T>
T& VectorDinamico<T>::operator[](int i)
{
	return m_ptr[i];
}



int main(int argc, char const *argv[])
{
	VectorDinamico<double> v1(10, 'a');

	v1[0] = -1;
	for (size_t i = 0; i < v1.get_size(); ++i)
	{
		cout << v1[i] << " ";
	}cout << endl;

	VectorDinamico<string> v2(10, "a");

	v2[0] = "hola";
	for (size_t i = 0; i < v1.get_size(); ++i)
	{
		cout << v2[i] << " ";
	}cout << endl;

return 0;
}