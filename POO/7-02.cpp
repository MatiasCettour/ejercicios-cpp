#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;
/*Implemente una función ​ Clamp(...) que reciba como parámetros una variable (por
referencia) y dos valores indicando los límites superior e inferior para el valor de
dicha variable. Si el valor de la variable supera su máximo, este debe ajustarse al
máximo valor permitido. De la misma forma si el valor es inferior al mínimo. Pruebe
la función templatizada desde un programa cliente. */

template<typename T>
void Clamp(T& ref, int max, int min)
{
	if (ref > max)
	{
		ref = max;
	}
	else if (ref < min)
	{
		ref = min;
	}
}

int main(int argc, char const *argv[])
{
	double a = 11; cout << a << endl;

		Clamp(a, 10, 0); cout << a << endl;

	a = -1;  cout << a << endl;

		Clamp(a, 5, 2);  cout << a << endl;
return 0;
}