#include <iostream>
using namespace std;

/*Se tiene un arreglo dinámico de ​ n enteros, y se quiere insertar al final ​ c ceros.
Implemente una función ​ redimensionar() que reserve dinámicamente un nuevo
arreglo que contenga lugar para guardar los ​ n datos anteriores y los ​ c ceros que
serán insertados, luego copie en la nueva memoria los datos del vector viejo y
agregue los ceros al final. La memoria del primer arreglo debe ser liberada y la
dirección del arreglo nuevo será retornada para que el programa cliente actualice
su puntero. La función debería poder invocarse de la siguiente manera:

					vector_int_dinamico arreglo; (Teoría Pág. 10)
					...
					arreglo = redimensionar( arreglo, c );
Hola! Feliz navidad! Recien volvi a la vida
Finalmente, implemente un programa cliente que muestre el arreglo resultante.*/

struct vector_int_dinamico
{
	int* p;
	size_t size;
};


vector_int_dinamico redimensionar(vector_int_dinamico* arreglo, int* C);

vector_int_dinamico generar_arreglo(int size);

void destuir_variables_dinamicas(vector_int_dinamico* arreglo);

void print_vector(vector_int_dinamico* vector);

//__________________________________________________________________________________________________________________________
int main(int argc, char const *argv[])
{
cout <<"Ingrese size vector > "; int n; cin >> n;

vector_int_dinamico array = generar_arreglo(n);

print_vector(&array);

	cout <<"Ingrese cantidad de 0's a insertar > "; int c; cin >> c;

	array = redimensionar(&array,&c);
	n += c;

	cout << endl << "Nuevo vector: ";
	print_vector(&array);

destuir_variables_dinamicas(&array);

return 0;
}
//____________________________________________________________________________________________________________________________

vector_int_dinamico generar_arreglo(int size)
{
	vector_int_dinamico new_array;

	new_array.size = size;
	new_array.p    = new int[size];

	for (size_t i = 0; i < size; ++i)
	{
		*(new_array.p+i) = rand() % 500 + 1000;
	}
return new_array;
}

void print_vector(vector_int_dinamico* arreglo)
{
	for (size_t i = 0; i < arreglo->size; ++i)
	{
		cout << arreglo->p[i] << " ";
	}
	cout << endl;
}

vector_int_dinamico redimensionar(vector_int_dinamico* arreglo, int* C)
{
	int* new_array = new int[arreglo->size+*C];

	for(int i = 0; i < arreglo->size; ++i) *(new_array+i) = *(arreglo->p+i);

								delete []arreglo->p;

	arreglo->p = new_array;

	for(int i = arreglo->size; i < arreglo->size+*C; ++i) arreglo->p[i] = 0; 

		arreglo->size += *C;

return *arreglo;
}

void destuir_variables_dinamicas(vector_int_dinamico* arreglo)
{
	delete []arreglo->p;
}