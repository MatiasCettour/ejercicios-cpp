#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;
/*Cree un programa que lea valores flotantes por teclado y los inserte en una lista.
Luego:
a. Muestre dicha lista.
b. Inserte en medio de cada par de elementos contiguos el promedio de
dichos elementos y guarde la lista resultante en un archivo de texto llamado
“listafloat.txt” ​ .
c. Responda: ¿es posible ordenar solamente una porción de la lista de la
misma manera que se hizo con el vector en el ejercicio 8.2?*/

int main(int argc, char const *argv[])
{
	
	list<float> l = {-10, 7, 9.5, -3, 4.5, -7, -5};
	
	/*cout << "Ingrese datos flotantes (-1 para salir)" << endl;
	float dato;
	do
	{
		cin >> dato;
		if (dato != -1) l.push_back(dato);
		
	}while(dato != -1);*/
	if (l.size() > 1)
	{
		//list<float>::iterator it = l.begin(); ++it;
		for (list<float>::iterator it = l.begin(); it != l.end(); ++it)
		{
			if (next(it) != l.end())
			{
				float promedio = (*it + *next(it)) / 2;
				l.insert(next(it), promedio);
				advance(it,2);
			}
		}
	}
	cout << endl;
	for (const float& x : l) cout << x << " ";
	cout << endl;
	
		ofstream file_out("lista3.txt", ios::trunc);

		for (const float& x : l) file_out << x << " ";

//c) No es posible ordenarlo como en 8.2 porque el iterador solo soporta
// los operadores ++ y -- (no + ni -)
return 0;
}
