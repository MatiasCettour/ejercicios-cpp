#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
using namespace std;
/*Escriba un programa que cargue en un vector de strings una lista de palabras
desde un archivo de texto (que contendrá una palabra por línea), muestre en
pantalla la cantidad de palabras leídas, las ordene en el vector alfabéticamente, y
reescriba el archivo original con la lista ordenada.
Ayuda: para ordenar un vector ​ v de principio a fin puede utilizar la sentencia
“​ sort(v.begin(),v.end()); ” ​.*/

int main(int argc, char const *argv[])
{
	int n=0;

	vector<string> v;
	string aux;

	ifstream arch_i("archivo.txt");

	while (arch_i >> aux)
	{    
		v.push_back(aux);
		n++;
	}
		cout << "Cantidad palabras: " << n;

arch_i.close();

			sort(v.begin(), v.end());

	ofstream arch_o("archivo.txt");

	for (size_t i = 0; i < n; ++i)
	{
		arch_o << v[i] << endl;
	}

return 0;
}