#include "iostream"
using namespace std;
/*Para la clase ​
Racional utilizada en el ejercicio anterior, implemente los operadores
relacionales <, <=, >, >=, == y != para comparar dos números racionales. Haga uso
de dichos operadores desde un programa cliente
*/

class Racional
{
public:
	Racional(double n, double d) : num(n), den(d){}

	double get_numerador() const { return num; }
	double get_denominador() const { return den; }

	Racional& operator++()
	{
		num = num + den;
	return *this;
	}

	Racional operator++(int)
	{
		Racional aux = *this;
		num = num + den;
	return aux;
	}

	Racional operator+ (const Racional& r2)
	{
		Racional suma((this->get_numerador()*r2.get_denominador()) + (r2.get_numerador()*this->get_denominador()),
								this->get_denominador()*r2.get_denominador()  );

	return suma;
	}

	Racional operator* (const Racional& r2)
	{
		Racional multiplicacion(this->get_numerador()*r2.get_numerador(),
								this->get_denominador()*r2.get_denominador());

	return multiplicacion;
	}

	bool operator< (const Racional& r2) { return (this->get_numerador() / this->get_denominador()) < (r2.get_numerador() / r2.get_denominador()); }
	bool operator> (const Racional& r2) { return (this->get_numerador() / this->get_denominador()) > (r2.get_numerador() / r2.get_denominador()); }
	bool operator<= (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) <= (r2.get_numerador() / r2.get_denominador()); }
	bool operator>= (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) >= (r2.get_numerador() / r2.get_denominador()); }
	bool operator== (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) == (r2.get_numerador() / r2.get_denominador()); }
	bool operator!= (const Racional& r2){ return (this->get_numerador() / this->get_denominador()) != (r2.get_numerador() / r2.get_denominador()); }
	
private: double num, den;
};



int main(int argc, char const *argv[])
{
	Racional a(3, 5), b(1, 3), c(1, 3);
	cout << (b < a) << endl;
	cout << (b < a) << endl;
	cout << (b < a) << endl;
	cout << (b < a) << endl;
	cout << (b < a) << endl;
	cout << (b < a) << endl;
	c = a+b;
	cout << c.get_numerador() << " " << c.get_denominador() << endl;
	c = a*b;
	c = a+b+c;
	cout << c.get_numerador() << " " << c.get_denominador() << endl;
	c = a+b+c;
	b = c++;
	a = ++c;
	cout << a.get_numerador() << " " << a.get_denominador() << endl;
	cout << b.get_numerador() << " " << b.get_denominador() << endl;
	return 0;
}