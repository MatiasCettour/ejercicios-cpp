#include <iostream>

using namespace std;
/*Escriba una función que utilice punteros para buscar e informar la dirección de un
entero dentro de un arreglo. Se pasan como parámetros el arreglo, su tamaño y el
entero a buscar. Si el dato no se encuentra, devolver la dirección de memoria nula
(​ nullptr o NULL). ¿Desde un programa cliente, cómo se obtiene el índice del
elemento encontrado cuando la función no devuelve n
ullptr​ ?*/

int* buscar(int arreglo[], size_t size, int entero)
{
	for (size_t i = 0; i < size; ++i)
	{
		if (arreglo[i] == entero) return &arreglo[i];
	}
return nullptr;
}

int main(int argc, char const *argv[])
{
	int x[10]={110, 120, 130, 140, 150, 160, 170, 180, 190, 200};

cout << buscar(x,10,111) << endl;

return 0;
}
