#include <iostream>
#include <vector>
using namespace std;

/*Se dispone del siguiente tipo de dato:
struct Alumno
{
	string nombre;
	float nota;
};
En base al mismo se desea crear una clase ​ Curso ​ para modelar el cursado de una
materia. La clase deberá contener el nombre de la materia y la cantidad de
alumnos en el curso junto con una lista de los mismos. Proponga los siguientes
métodos:
a. Constructores y destructores según lo considere conveniente.
b. Un método que permita agregar un Alumno.
c. Un método que determine el promedio del curso.
d. Un método que devuelva la calificación más alta y el nombre del alumno
que la obtuvo. */

struct Alumno { string nombre; float nota; };

class Curso
{
public:
	Curso(): m_alumnos(new Alumno[0]) {}

	void agregar_alumno(string name, float nota);
	float get_promedio();
	Alumno get_mejor_alumno();
	string get_materia();

	~Curso(){  delete[] m_alumnos; }

private:
	string m_nombre_materia;
	int m_cantidad_alumnos;
	Alumno* m_alumnos;
};

void Curso::agregar_alumno(string name, float nota)
{
	Alumno* new_curso = new Alumno[m_cantidad_alumnos+1];

		for (int i = 0; i < m_cantidad_alumnos; ++i)
		{
			new_curso[i] = m_alumnos[i];
		}

		new_curso[m_cantidad_alumnos] = {name, nota};

			++m_cantidad_alumnos;
			delete[] m_alumnos;

				m_alumnos = new_curso;
}

float Curso::get_promedio()
{
	float promedio = 0;

		for (int i = 0; i < m_cantidad_alumnos; ++i)
		{
			promedio += m_alumnos[i].nota;
		}
				promedio /= m_cantidad_alumnos;
}

Alumno Curso::get_mejor_alumno()
{
	Alumno mejor {"",0.f};

		for (int i = 0; i < m_cantidad_alumnos; ++i)
		{
			if (m_alumnos[i].nota > mejor.nota) mejor = m_alumnos[i];
		}
				return mejor;
}

string Curso::get_materia() { return m_nombre_materia; }
