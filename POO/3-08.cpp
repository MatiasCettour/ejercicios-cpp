#include <iostream>
using namespace std;
/*Una fábrica de Tanques los hace de forma de Cilindro o de Esfera, en ambos
envases debemos rotular el volumen en m3 y el peso en kilogramos.
Modele una clase base Tanque con los atributos volumen y peso. Un método
público AsignarPeso(p), un método virtual puro CalcularVolumen() que calcule el
volumen de acuerdo a los parámetros de los hijos y otros 2 métodos para
VerVolumen() y VerPeso().
Modele la clase hija Cilindro que tendrá los atributos radio y altura, cuya fórmula de
volumen es: área de la base x altura, donde el area de la base se calcula como
PI*radio^2; y otra clase hija Esfera que tendrá el atributo radio, cuya fórmula de
volumen es: 4/3*PI*radio^3. Los atributos (medidas y peso) los debe cargar con un
constructor.
En el programa principal debe usar un único puntero de tipo Tanque para crear
primero un Cilindro y mostrar su volúmen, y luego una Esfera y también mostrar su
volúmen.*/

class Tanque
{
public:

	void set_peso(int p){ m_peso = p; }
	void set_volumen(int p){ m_volumen = p; }

	virtual void calcular_volumen()=0;

	float get_volumen(){ return m_volumen; }
	float get_peso()   { return m_peso;    }

private:
	float m_peso, m_volumen;	
};

class Cilindro : public Tanque
{
public:
	Cilindro(float radio, float altura): m_radio(radio), m_altura(altura){}

	void calcular_volumen() override {  set_volumen (3.14*m_radio*m_radio*m_altura); }

private: float m_radio, m_altura;
};

class Esfera : public Tanque
{
public:
	Esfera(float radio): m_radio(radio){}

	void calcular_volumen() override {  set_volumen (4/3*3.14*m_radio*m_radio*m_radio); }

private: float m_radio;
};


int main(int argc, char const *argv[])
{
	Tanque* c1 = new Cilindro(2,6);
	c1->calcular_volumen();

	cout << c1->get_volumen() << "\n\n";

	Tanque* e1 = new Esfera(6);
	e1->calcular_volumen();

	cout << e1->get_volumen() << "\n\n";
	
	return 0;
}