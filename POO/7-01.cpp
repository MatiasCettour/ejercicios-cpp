#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;
/*Implemente una función templatizada llamada ​ Mayor(...) que reciba dos valores y
devuelva el mayor. Compruebe el correcto funcionamiento de la rutina probándola
desde un programa cliente con valores de tipo ​ int ​ , ​ float ​ y ​ string ​ .
a. Programe una sobrecarga de la función ​ Mayor(...) que reciba un vector y
retorne el mayor elemento del mismo. Pruebe la función sobrecargada
desde un programa cliente con diversos tipos de datos.
b. Responda: ¿Servirán las funciones anteriores con datos del tipo mostrado
en el recuadro? Justifique su respuesta.

struct​​ Persona{
string nombre;
string apellido;
int​​ dni;
};

c. Implemente los cambios necesarios, utilizando su propio criterio, para que
las funciones desarrolladas funcionen con el tipo de dato del recuadro. */

struct Persona
{
	string nombre;
	string apellido;
	int dni;
};

template <typename T>
T Mayor(T f1, T f2)
{
	if (f1 > f2) return f1;
	else return f2;
}
template <typename Tfun>
Persona Mayor(Persona p1, Persona p2, Tfun es_mayor)
{
	if (es_mayor(p1,p2)) return p1;
	else return p2;
}

bool comparar_nombre(const Persona& a1, const Persona& a2)
{
	return a1.nombre > a2.nombre;
}
bool comparar_apellido(const Persona& a1, const Persona& a2)
{
	return a1.apellido > a2.apellido;
}
bool comparar_dni(const Persona& a1, const Persona& a2)
{
	return a1.dni > a2.dni;
}

int main(int argc, char const *argv[])
{
	string a = "a", b = "b";
	string m = Mayor<string> (a,b);  cout << m << endl;

	float x = Mayor(2.3, 5.7);  cout << x << endl;

	int x2 = Mayor(2, 5);  cout << x2 << endl;

	Persona matias    = {"Matias", "Cettour", 41268236};
	Persona alejandro = {"Alejandro", "Battistte", 21268236};

	cout << "Comparacion nombre: "   << Mayor (matias, alejandro, comparar_nombre).nombre     << endl;
	cout << "Comparacion apellido: " << Mayor (matias, alejandro, comparar_apellido).apellido << endl;
	cout << "Comparacion dni: "      << Mayor (matias, alejandro, comparar_dni).dni 		  << endl; 

return 0;
}