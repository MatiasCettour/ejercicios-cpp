#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
using namespace std;
/*En un archivo de texto llamado lista.txt ​ , como el que se
muestra a la derecha, se encuentran los nombres y notas
de los alumnos de una comisión de Programación
Orientada a Objetos.
a) Escriba una función modificar que reciba el nombre
de un alumno y dos notas, y modifique el archivo
reemplazando las dos notas de ese alumno.
b) Escriba una función que lea la lista del archivo y genere otro archivo de texto
promedios.txt c ​ on una tabla donde cada línea posea el nombre, el promedio,
y la condición final de cada uno de los alumnos.
Ayuda: utilice manipuladores de flujo (​ setw ​ , ​ right , ​ ​ left ​ , ​ fixed ​ , ​ setprecision ) ​ para dar
formato a la tabla del archivo que se genera en b) */

struct Alumno { string nombre; int nota1, nota2; };

void modificar_nota(string __nombre, int __nota1, int __nota2)
{
	ifstream arch_i("archivo.txt");
	Alumno aux;
	vector<Alumno> v;
	size_t i = 0, pos = 0;


	while (getline(arch_i, aux.nombre) && arch_i >> aux.nota1 >> aux.nota2)
	{   
		if (aux.nombre == __nombre) pos = i;
		v.push_back(aux);
		arch_i.ignore();
		++i;
	}

arch_i.close();

		v[pos] = {__nombre, __nota1, __nota2};

			ofstream arch_o("archivo.txt");

			for (i = 0; i < v.size(); ++i)
			{
				arch_o << v[i].nombre << endl << v[i].nota1 << " " << v[i].nota2 << endl;
			}
}

int main(int argc, char const *argv[])
{

modificar_nota("Garcia Ana", 90, 90);

return 0;
}
/*
Lopez Javier
56 90
Garcia Ana
71 81
Farias Daniel
60 62
*/