#include <iostream>
using namespace std;
/* Diseñe una clase ​ Cilindro que modele un cilindro con el objetivo de calcular el
volumen del cuerpo conociendo el radio de su base y la altura del mismo.
a. Cree los métodos ​ AsignarDatos(...) y ​ ObtenerVolumen(...) para asignar los
datos del problema y obtener el volumen del cuerpo.
b. Escriba un programa cliente que utilice dicha clase. Defina 2 instancias de
Cilindro llamadas ​ c1 y ​ c2 . ​ El objeto ​ c1 debe utilizar datos ingresados por el
usuario, mientras que para ​ c2 utilice 5.3 cm y 10.2 cm para el radio y la
altura respectivamente.
c. Agregue un constructor a la clase ​ Cilindro que reciba 2 parámetros para
inicializar el radio y la altura. Luego intente compilar nuevamente el
programa. ¿Puede explicar por qué se produce un error? Proponga una
solución. */

class Cilindro
{
public:

	Cilindro() : m_radio(5.3), m_altura(10.2) {}

	void asignar_datos(float radio, float altura);
	float get_volumen() const;

	~Cilindro(){};

private:

	float m_radio, m_altura;
};

void Cilindro::asignar_datos(float radio, float altura)
{
	m_radio  = radio;
	m_altura = altura; 
}
float Cilindro::get_volumen() const
{
	return 3.14*m_radio*m_radio*m_altura; //pi*r^²*h
}
//------------------------------------------------------------------------------------------
int main()
{
Cilindro c1, c2;
float r,h;

	cout << "Ingrese radio de c1 > "; cin >> r;
	cout << "Ingrese altura de c1 > "; cin >> h;

	c1.asignar_datos(r,h);

		cout <<"\nVolumen c1: "<< c1.get_volumen();
		cout <<"\nVolumen c2: "<< c2.get_volumen();

cout << endl;
return 0;
}
