#include <iostream>
using namespace std;
/*a. Defina una clase Tecla para representar una tecla de un piano. Cada tecla
puede estar o no apretada, y tiene además una nota asociada (cuyo nombre se
representará con un string). Su interfaz debe tener tener entonces:
- un constructor que reciba el nombre de la nota
- un método VerNota que retorne el nombre de la nota
- un método Apretar que cambie el estado de la tecla a apretada.
- un método Soltar que cambie el estado de la tecla a no apretada.
- un método EstaApretada que retorne true si la tecla está apretada, false en caso
contrario

b. Defina una clase ​ Pedal ​ para representar el pedal de un piano. El pedal debe
almacenar un valor (​ float ​ ) que indica la presión que el músico ejerce sobre el pedal.
El constructor debe inicializar la presión en ​ 0 ​ , y la clase debe tener métodos para
modificarla y consultarla.

c. Reutilizando las clases ​ Tecla , ​ Pedal e Instrumento:

class Instrumento{
public:
virtual string VerTipo() { return "sin_nombre"; }
};

defina una clase ​ Piano ​ que modele un instrumento de tipo ​ "piano" con solo 7 teclas
(“​ do”, “re”, “mi”, “fa”, “sol”, “la” y “si” ) ​ y 1 pedal. La clase piano debe tener métodos
para:
- apretar una tecla, indicando el número de tecla, y que retorne la nota que debería
sonar.
- soltar una tecla, indicando el número de tecla
- presionar el pedal, indicando la presión que se aplica*/

class Tecla
{
public:
	Tecla(string nombre_nota) : m_nombre_nota(nombre_nota){}

	string get_nota(){ return m_nombre_nota; }
	void apretar() { m_estado = 1; }
	void soltar()   { m_estado = 0; }
	bool esta_apretada(){ return m_estado == 1; }

private:
	string m_nombre_nota;
	bool m_estado = 0;
};

class Pedal
{
public:
	Pedal() : m_presion(0) {}

	void set_presion(float valor){ m_presion = valor; }
	float get_presion(){ return m_presion; }
	
private: float m_presion;
};

class Instrumento
{
	public: virtual string ver_tipo() { return "sin_nombre"; }
};	

class Piano : public Instrumento
{
public:
	virtual string ver_tipo() override { return "Piano"; }
	string apretar_tecla(int tecla) { teclas[tecla].apretar(); return teclas[tecla].get_nota();}
	void solar_tecla(int tecla){ teclas[tecla].soltar(); }
	void ajustar_pedal(float presion){ m_pedal.set_presion(presion); }

private:
	Tecla teclas[7] = {{"do"}, {"re"}, {"mi"}, {"fa"}, {"sol"}, {"la"}, {"si"}};
	Pedal m_pedal;
};


int main(int argc, char const *argv[])
{
	Piano piano;

	cout << piano.apretar_tecla(1) <<endl;
	piano.ajustar_pedal(8.5);
	cout << piano.ver_tipo()<<endl;

return 0;
}
