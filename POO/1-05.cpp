#include <iostream>

using namespace std;
/*Utilizando notación de punteros,generar un arreglo dinámico y lineal de N elementos numéricos enteros,
con valores aleatorios entre 1000 y 1500, y muestre
en pantalla la dirección de memoria del mayor elemento. N es un dato ingresado por el usuario*/

int* get_mayor(int* vector, size_t size);

int* generar_arreglo(size_t size);

void destuir_variables_dinamicas(int* arreglo, int* mayor);

void print_vector(int* vector, size_t size);

int main(int argc, char const *argv[])
{

int n; cin >> n;

int* arreglo = generar_arreglo(n);
int* v = arreglo;

print_vector(arreglo,n);

int* mayor = get_mayor(arreglo,n);

cout << *mayor;

destuir_variables_dinamicas(arreglo, mayor);

print_vector(v,n);
return 0;
}


int* generar_arreglo(size_t size)
{
	int* arreglo = new int[size];

	for (size_t i = 0; i < size; ++i)
	{
		*(arreglo+i) = rand() % 500 + 1000;
	}
return arreglo;
}

void print_vector(int* arreglo, size_t size)
{
	for (size_t i = 0; i < size; ++i)
	{
		cout << arreglo[i] << " ";
	}cout << endl;
}

int* get_mayor(int* arreglo, size_t size)
{
int* mayor = new int;
*mayor     = 999;

	for (size_t i = 0; i < size; ++i)
	{
		if(*(arreglo+i) > *mayor) *mayor = *(arreglo+i);
	}
return mayor;
}

void destuir_variables_dinamicas(int* arreglo, int* mayor)
{
	delete [] arreglo;
	delete mayor;
}