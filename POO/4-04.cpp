#include "iostream"
using namespace std;
/*Implemente una clase llamada ​
Complejo para representar un número complejo.
Sobrecargue los operadores <<, >>, +, ­- , * e == para mostrar, leer, sumar, restar,
multiplicar y comparar respectivamente dos objetos de tipo Complejo. Compruebe
el funcionamiento de los operadores desde un programa cliente. */

class Complejo
{
public:
	Complejo(double real, double imag) : m_real(real), m_imag(imag){}

	double get_real()const{ return m_real; }
	double get_imag()const{ return m_imag; }
	
	Complejo(const Complejo& com)
	{
		m_real = com.get_real();
		m_imag = com.get_imag();
	}

	Complejo operator+(const Complejo& c)
	{
		Complejo suma{ this->get_real()+c.get_real(), this->get_imag()+c.get_imag() };

	return suma;
	}

	Complejo operator-(const Complejo& c)
	{
		Complejo resta{ this->get_real()-c.get_real(), this->get_imag()-c.get_imag() };

	return resta;
	}

	Complejo operator*(const Complejo& c)
	{
		Complejo mult{ this->get_real()*c.get_real() - this->get_imag()*c.get_imag(), this->get_real()*c.get_imag() + this->get_imag()*c.get_real() };

	return mult;
	}

	bool operator==(const Complejo& c)
	{
		return this->get_real() == c.get_real() && this->get_imag() == c.get_imag();
	}

private: double m_real, m_imag;
};

ostream& operator<<(ostream& o, Complejo c)
{
	if (c.get_imag() >= 0) o << c.get_real() << " + " << c.get_imag();
	else o << c.get_real() << " " << c.get_imag();

return o;
}
istream& operator>>(istream& i, Complejo& c)
{
	double re; i >> re;
	double im; i >> im;

		c = {re,im};
return i;
}