#include "iostream"
using namespace std;
/*Implemente una sobrecarga del operador[ ] para la clase ​
Complejo que permita
obtener las partes real e imaginaria como si fueran 2 elementos en un arreglo:
Explique: ¿Podría su sobrecarga utilizarse para modificar las partes del número
complejo? */

class Complejo
{
public:
	Complejo(double real, double imag) : m_real(real), m_imag(imag){}

	double get_real()const{ return m_real; }
	double get_imag()const{ return m_imag; }
	
	Complejo(const Complejo& com)
	{
		m_real = com.get_real();
		m_imag = com.get_imag();
	}

	Complejo operator+(const Complejo& c)
	{
		Complejo suma{ this->get_real()+c.get_real(), this->get_imag()+c.get_imag() };

	return suma;
	}

	Complejo operator-(const Complejo& c)
	{
		Complejo resta{ this->get_real()-c.get_real(), this->get_imag()-c.get_imag() };

	return resta;
	}

	Complejo operator*(const Complejo& c)
	{
		Complejo mult{ this->get_real()*c.get_real() - this->get_imag()*c.get_imag(), this->get_real()*c.get_imag() + this->get_imag()*c.get_real() };

	return mult;
	}

	bool operator==(const Complejo& c)
	{
		return this->get_real() == c.get_real() && this->get_imag() == c.get_imag();
	}

	double operator[] (int i) const
	{
		if (i == 0) return this->get_real();
		else 		return this->get_imag();
	}

	double& operator[] (int i) 
	{
		if (i == 0) return m_real;
		else 		return m_imag;
	}

private: double m_real, m_imag;
};

ostream& operator<<(ostream& o, Complejo c)
{
	if (c.get_imag() >= 0) o << c.get_real() << " + " << c.get_imag() <<"i"<< endl;
	else o << c.get_real() << " " << c.get_imag() <<"i"<< endl;

return o;
}
istream& operator>>(istream& i, Complejo& c)
{
	double re; i >> re;
	double im; i >> im;

		c = {re,im};
return i;
}

int main(int argc, char const *argv[])
{
	Complejo c1{1,2};
	cin >> c1;
	cout << c1;
	cout << c1[0] << " " << c1[1] <<endl;
	c1[0] = 0;
	cout << c1[0] << " " << c1[1] <<endl;

return 0;
}