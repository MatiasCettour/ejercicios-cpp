#include <iostream>

using namespace std;

class Vector
{
public:

protected:
	int *m_p;
	int a;
};

class A : public Vector
{
public:

	void func()
	{
		a = 1;
	}

};

int main()
{
int n = 1;
int number_of_digits = 0;

	do 
	{
		cout << n << endl; 
		++number_of_digits; 
		n /= 10;
	} while (n);

cout << number_of_digits;
return 0;
}
