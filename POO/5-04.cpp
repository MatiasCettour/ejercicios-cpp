#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <string>
using namespace std;
/*Un conjunto de archivos de texto contiene información guardada en el formato que
se muestra en el recuadro, donde cada línea contiene el nombre de un campo y su
respectivo valor separados por el signo igual (=). Las líneas que comienzan con el
carácter '#' deben ser ignoradas.

	#ejemplo de archivo de configuracion
	materia=Programacion orientada a objetos
	carrera = Ingeniera en informatica
	facultad = Facultad de ingenieria y ciencias hidricas
	universidad = Universidad nacional del litoral
	#esta linea hay que ignorarla, igual que a las 3 ultimas
	nro_unidades = 5
	nombre_unidad = Flujos de entrada/salidad
	#campo_que_no_se_lee = basura
	#nada
	#nada

Escriba una clase llamada ConfigFile que permita interpretar el contenido de estos
archivos. La clase debe poseer:
a) un constructor que reciba el nombre del archivo y cargue sus datos en un
vector de structs (cada elemento es struct con dos strings: campo y valor).
b) un método para consultar el valor asociado a un campo
c) un método para modificar el valor asociado a un campo
d) un método para guardar el archivo con los datos actualizados
 */

class Config_file
{	
public:
	Config_file(string __file);

	string get_carrera(){ 		return m_carrera; }
	string get_materia(){ 		return m_materia; }
	string get_facultad(){ 		return m_facultad; }
	string get_universidad(){ 	return m_universidad; }
	string get_nro_unidades(){  return m_nro_unidades; }
	string get_nombre_unidad(){ return m_nombre_unidad; }

	void set_carrera	  (string __dato){ m_carrera 	   = __dato; }
	void set_materia	  (string __dato){ m_materia 	   = __dato; }
	void set_facultad	  (string __dato){ m_facultad 	   = __dato; }
	void set_universidad  (string __dato){ m_universidad   = __dato; }
	void set_nro_unidades (string __dato){ m_nro_unidades  = __dato; }
	void set_nombre_unidad(string __dato){ m_nombre_unidad = __dato; }

	void crear_archivo(string __nombre)
	{
		ofstream arch_o(__nombre);

		arch_o << "carrera = " << 		get_carrera() << endl;
		arch_o << "materia = " << 		get_materia() << endl;
		arch_o << "facultad = " << 		get_facultad() << endl;
		arch_o << "universidad = " <<   get_universidad() << endl;
		arch_o << "nro_unidades = " <<  get_nro_unidades() << endl;
		arch_o << "nombre_unidad = " << get_nombre_unidad() << endl;
	}

private:
	string m_materia, m_carrera, m_facultad, m_universidad, m_nro_unidades, m_nombre_unidad;

	string linea;
	size_t p = 0;

	void set_cursor(size_t __cursor)
	{
		++__cursor;
		while (linea.substr(__cursor, 1) == " " or linea.substr(__cursor, 1) == "	") ++__cursor;

			p = __cursor;
	}
};

int main(int argc, char const *argv[])
{
	Config_file poo("config.txt");

	cout << poo.get_carrera() << endl;
	cout << poo.get_materia() << endl;
	cout << poo.get_facultad() << endl;
	cout << poo.get_universidad() << endl;
	cout << poo.get_nro_unidades() << endl;
	cout << poo.get_nombre_unidad() << endl;

	poo.set_facultad("UTN");
	poo.crear_archivo("04");

return 0;
}

Config_file::Config_file(string __file)
{
	ifstream arch_i(__file);

		while (getline(arch_i, linea))
		{
			p=0;
			while (linea.substr(p, p+1) == " " or linea.substr(p, p+1) == "	")
			{
				++p;
			}

				if (linea.substr(p,1) == "#") continue;

				if (linea.substr(p, 7) == "carrera")
				{
					 p = linea.find("=");
					if (p == string::npos) continue;
					else
					{	
						set_cursor(p);
					
						m_carrera = linea.substr(p);
					}
				}
				else if (linea.substr(p, 7)  == "materia")
				{
					 p = linea.find("=");
					if (p == string::npos) continue;
					else
					{
						set_cursor(p);
						m_materia = linea.substr(p);
					}
				} 
				else if (linea.substr(p, 8)  == "facultad")
					{
					 p = linea.find("=");
					if (p == string::npos) continue;
					else
					{
						set_cursor(p);
						m_facultad = linea.substr(p);
					}
				}
				else if (linea.substr(p, 11) == "universidad")
				{
					 p = linea.find("=");
					if (p == string::npos) continue;
					else
					{
						set_cursor(p);
						m_universidad = linea.substr(p);
					}
				}
				else if (linea.substr(p, 12) == "nro_unidades")
				{
					 p = linea.find("=");
					if (p == string::npos) continue;
					else
					{
						set_cursor(p);
						m_nro_unidades = linea.substr(p);
					}
				} 
				else if (linea.substr(p, 13) == "nombre_unidad")
				{
					 p = linea.find("=");
					if (p == string::npos) continue;
					else
					{
						set_cursor(p);
						m_nombre_unidad = linea.substr(p);
					}
				}

		}
}