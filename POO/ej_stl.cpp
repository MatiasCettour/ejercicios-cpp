#include <iostream>
#include <vector>
#include <list>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;

int main(int argc, char const *argv[])
{
	/*list<int> L;
	for (int i = 0; i < 15; ++i)
	{
		L.push_back(1+rand()%100);
	}

	for (const int& x : L) cout << x << " ";
cout << endl;

	int pos; cin >> pos;
	auto it = L.begin(); //auto es list<int>::iterator
	advance(it, pos);
	L.erase(it);

	for (const int& x : L) cout << x << " ";
cout << endl;*/

/*Escriba un programa que permita ingresar un
conjunto de mediciones.
Por error en el sensor, algunas mediciones no
pudieron realizarse y en su lugar se registró un -1.
Reemplace todos los -1 por el promedio de los
valores adyacentes.
Ayuda: Se sabe que no hay dos -1 consecutivos, y
que tampoco están al comienzo o al final de la
lista.*/

	/*list<int> L;
	for (int i = 0; i < 10; ++i)
	{
		int dato; cin >> dato;
		L.push_back(dato);
	}

		for (auto it = L.begin(); it != L.end(); ++it)
		{
			if (*it == -1)
			{
				*it = (*next(it))+*prev(it)/2;
			}
		}for (const int& x : L) cout << x << " ";
cout << endl;*/


	/*Escriba un programa que permita ingresar una
lista de valores flotantes por teclado, y luego
inserte en medio de cada par de elementos
consecutivos el promedio del par.*/

	/*list<float> L;
	for (int i = 0; i < 6; ++i)
	{
		float dato; cin >> dato;
		L.push_back(dato);
	}

		for( auto it = next(L.begin()); L.end(); advance(it,2) )
		{
			float prom = (*it + *next(it)) / 2;
			it = L.insert(next(it), prom);

		}
for (const float& x : L) cout << x << " ";
cout << endl;*/


/*Genere una cantidad arbitraria de valores enteros (n>=1)
aleatorios.
	int rand_20() 
	{
		return rand()%20;
	}
	int main() 
	{
		int n;
		cout << "Cant. de datos a generar: ";
		cin >> n;
		list<int> L(n);
		generate(L.begin(),L.end(),rand_20);
		...

Genere una cantidad arbitraria de valores enteros (n>=1)
aleatorios y muestre:

la lista de datos inciales
for(int x:L) cout << x;

el promedio
float sum=accumulate(L.begin(),L.end(),0);
cout << sum/L.size() << endl;

los valores mínimo y máximo
auto it_max=max_element(L.begin(),L.end());
auto it_min=min_element(L.begin(),L.end());
cout << *it_max << " " << *it_min << endl;

Genere una cantidad arbitraria de valores enteros (n>=1)
aleatorios y muestre:

la lista ordenada de menor a mayor
L.sort();
for(int x:L) cout << x << " ";

la mediana
auto itm = next(L.begin(),L.size()/2);
cout << *itm << endl;

la lista ordenada de mayor a menor
reverse(L.begin(),L.end());
for(int x:L) cout << x << " ";

Genere una cantidad arbitraria de valores enteros (n>=1)
aleatorios y muestre:

la cantidad de ceros
cout << count(L.begin(),L.end(),0);

la cantidad de primos
bool es_primo(int x) { ... }
cout<<count_if(L.begin(),L.end(),es_primo);

la posición del valor 7
auto it_7 = find(L.begin(),L.end(),7);
if(it_7==L.end()) cout << "No está";
else cout << "Está en la pos: "
<< distance(L.begin(),it_7);

Genere una cantidad arbitraria de valores enteros (n>=1)
aleatorios y muestre:

la lista de valores sin elementos repetidos
L.sort(); // L debe estar ordenada
auto it_rep = unique(L.begin(),L.end());
L.erase(it_rep,L.end()); // unique no puede eliminar

la lista de valores sin ceros
auto it_0 = remove(L.begin(),L.end(),0);
L.erase(it_0,L.end());

la lista de valores sin primos
auto it_pr = remove_if(L.begin(),L.end(),
es_primo);
L.erase(it_pr,L.end());
		*/




	return 0;
}

/*Escriba una función que reciba el nombre de un
archivo binario que contenga un conjunto de
registros de un tipo genérico, y elimine del archivo
todos los registros repetidos. La función debe
retornar la cantidad de elementos eliminados.
template<typename T>
int elimina_reps(string file_name)
{
	ifstream file_in(file_name, ios::binary|ios::ate);
	int n = file_in.tellg()/sizeof(T);

	vector<T> v(n);

	file_in.seekg(0);
	for(T& x : v)
	{
		file_in.read(reinterpret_cast<char*>(&T), sizeof(T));
	}
	file_in.close();

	int cantidad = 0;
	for (size_t i = 0; i < v.size(); ++i)
	{
		auto it2 = remove(v.begin()+i+1, v.end(), v[i]);
		cantidad += v.end() - it2;
		v.erase(it2, v.end());
	}

	ofstream file_out(file_name, ios::binary|ios::trunc);
	for(T& x : v)
	{
		file_out.write(reinterpret_cast<char*>(&T), sizeof(T));
	}
return cantidad;
}*/

/*Un archivo de texti "medallas.txt" contiene una 
linea por cada pais que participa en los juegos 
olimpicos. Cada linea incluye, separados por
espacios primero 3 valores enteros y luego el 
nombre del pais

Escriba una funcion que reciba los nombres de los
paises que representan los tres ganadores de 
medallas de una competencia y actualice el archivo*/

void actualizar_medallero(string prim, string seg, string ter)
{
	struct S
	{
		s(){ oro=plata=bronce=0; }
		int oro, plata, bronce;
	};

	map<string, S> m;

	ifstream file_in("medallas.txt");
	string n; int o,p,b;

	while (file_in >> 0 >> p >> b && getline(file_in, n))
	{
		m[n].oro = o;
		m[n].plata = p;
		m[n].bronce = b;
	}

	file_in.close();

	m[prim].oro++;
	m[seg].plata++;
	m[ter].bronce++;

	ofstream file_out("medallas".txt, ios::trunc);

	for(auto& p : m)
	{
		file_out << p->second.oro << " ";
		file_out << p->second.plata << " ";
		file_out << p->second.bronce << " ";
		file_out << p->first << " ";
	}
}