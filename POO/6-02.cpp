#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <string>
using namespace std;
/*Escriba un programa abra el archivo generado en el ejercicio anterior y solicite al
usuario que ingrese un entero, un flotante y una posición. El programa debe
sobrescribir el par en la posición ingresada por el usuario por el nuevo par. Luego
muestre la lista de datos en consola mostrando un par por línea. */

struct Par { int entero; float flotante; };

int main(int argc, char const *argv[])
{
	fstream arch("grupo.bin", ios::binary | ios::in | ios::out | ios::ate);

	int tam_bytes = arch.tellg();
	int n         = tam_bytes/sizeof(Par);

	Par aux; int p;

	cin >> aux.entero >> aux.flotante >> p;

	arch.seekp(p*sizeof(Par));
	arch.write(reinterpret_cast<char*>(&aux), sizeof(aux));

	arch.seekg(0);

	for (int i = 0; i < n; ++i)
	{
		arch.read(reinterpret_cast<char*>(&aux), sizeof(aux));

		cout << aux.entero << " " << aux.flotante << endl;
	}

return 0;
}