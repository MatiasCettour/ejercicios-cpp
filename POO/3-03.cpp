#include <iostream>
#include <string>
using namespace std;

/*Proponga un struct ​ Punto con atributos para definir un punto en el plano
(coordenadas x e y). Luego, proponga la clase ​ RectaExplicita para definir la
ecuación de la recta ​ y=mx+b a partir de dos puntos. La declaración de dicha clase
se muestra en el recuadro siguiente

El método ​ ObtenerEcuacion() debe devolver una cadena de texto con la ecuación
explícita de la recta.*/

struct Punto
{
	float x = 0;
	float y = 0;
};

class Recta_explicita
{
public:
	Recta_explicita(Punto &p1, Punto &p2);
	string get_ecuacion();
	float print_m(){ cout << "m = " << m; }
	float print_b(){ cout << "b = " << b; }

private: float m; float b;
};

Recta_explicita::Recta_explicita(Punto &p1, Punto &p2)
{
	m = (p2.y - p1.y) / (p2.x - p1.x);

	b = p1.y - m*(p1.x);
}

string Recta_explicita::get_ecuacion()
{
	if (b < 0) return "y = " + to_string(m) + "x " + to_string(b);
	else return "y = " + to_string(m) + "x + " + to_string(b);
	
}


int main(int argc, char const *argv[])
{
	Punto p1 = {2,1};
	Punto p2 = {-1, -5};
	Recta_explicita r1(p1,p2);

		cout <<r1.get_ecuacion()<<endl;

	return 0;
}