#include "iostream"
using namespace std;
/*Dada la clase ​
Racional​
mostrada en el recuadro:

class Racional
{
public:
	Racional(int n, int d) : num(n), den(d){}

	int get_num(){ return num; }
	int get_den(){ return den; }
	
private: int num, den;
};
Implemente sobrecargas para los siguiente operadores:
● El operador + para sumar dos objetos de la clase R
​ acional​
.
● El operador * para multiplicar un objeto de tipo R
​ acional​
por un entero.
● El operador ++ que permita incrementar en una unidad un número racional.

Finalmente, compruebe el funcionamiento de los operadores con el siguiente
programa cliente:

int main(int argc, char const *argv[])
{
	Racional a(3, 5), b(2, 3), c(0, 1);
	c = a+b;
	cout << c.get_num() << " " << c.get_den() << endl;
	c = a*b;
	c = a+b+c;
	c = a+b+c;
	b = c++;
	a = ++c;
	cout << a.get_num() << " " << a.get_den() << endl;
	cout << b.get_num() << " " << b.get_den() << endl;
	return 0;
}
Si ocurren errores de compilación, explique su causa e implemente las
correcciones necesarias.
Analice: ¿Qué otro operador se utiliza para la clase ​
Racional en éste programa
cliente? ¿Por qué no es necesario sobrecargarlo?
*/

class Racional
{
public:
	Racional(int n, int d) : num(n), den(d){}

	int get_numerador() const { return num; }
	int get_denominador() const { return den; }

	Racional& operator++()
	{
		num = num + den;
	return *this;
	}

	Racional operator++(int)
	{
		Racional aux = *this;
		num = num + den;
	return aux;
	}

	Racional operator+ (const Racional& r2)
	{
		Racional suma((this->get_numerador()*r2.get_denominador()) + (r2.get_numerador()*this->get_denominador()),
								this->get_denominador()*r2.get_denominador()  );

	return suma;
	}

	Racional operator* (const Racional& r2)
	{
		Racional multiplicacion(this->get_numerador()*r2.get_numerador(),
								this->get_denominador()*r2.get_denominador());

	return multiplicacion;
	}
	
private: int num, den;
};



int main(int argc, char const *argv[])
{
	Racional a(3, 5), b(2, 3), c(0, 1);
	c = a+b;
	cout << c.get_numerador() << " " << c.get_denominador() << endl;
	c = a*b;
	c = a+b+c;
	cout << c.get_numerador() << " " << c.get_denominador() << endl;
	c = a+b+c;
	b = c++;
	a = ++c;
	cout << a.get_numerador() << " " << a.get_denominador() << endl;
	cout << b.get_numerador() << " " << b.get_denominador() << endl;
	return 0;
}