#include <iostream>
#include <vector>
using namespace std;

/*Diseñe una clase ​ Polinomio cuyos atributos sean el grado (entero) y los
coeficientes de los términos (hasta 20 términos; utilice un vector estático o la clase
vector, pero no new/delete). La clase debe tener:
a. un constructor que reciba el grado el polinomio e inicialice sus coeficientes
en 0.
b. un método que permita cambiar un coeficiente.
c. un método evaluar que permita evaluar el polinomio para un valor dado de
la variable ​ x . ​
d. el/los métodos que considere necesarios para poder mostrar un polinomio
desde un programa cliente.
e. una función que permita sumar dos polinomios retornando un tercer
polinomio con el resultado (ej. p_res=Sumar(p1,p2);). ¿Cómo variaría si en
lugar de ser una funciń libre fuera un método de la clase?
Verifique el funcionamiento de la función ​ Sumar(...) ​ mediante un programa cliente.*/

class Polinomio
{
public:

	Polinomio(const Polinomio& old_pol)
	{
		m_grado = old_pol.get_grado();

		for (int i = 0; i < m_grado; ++i)
		{
			m_terminos[i] = old_pol.get_coeficiente(i);
		}
	}

	Polinomio(int grado)
	{
		m_grado = grado;
		inicializar_coeficientes();
	}

	void cambiar_coeficiente(int coeficiente, int valor);
	int evaluar(int x);
	void print_polinomio();
	int get_grado() const;
	int get_coeficiente(int pos) const;

	~Polinomio(){};

private:

	int m_grado = 0; 
	int	m_terminos[20];
	void inicializar_coeficientes();
};

Polinomio sumar_polinomios(Polinomio* p1, Polinomio* p2);
int calcular_exponente(int base, int exponente);


int main(int argc, char const *argv[])
{
	Polinomio p(3);

		p.cambiar_coeficiente(3,-2);
		p.cambiar_coeficiente(2,3);
		p.cambiar_coeficiente(1,-5);
		p.cambiar_coeficiente(0,1);

		p.print_polinomio();

		cout << p.evaluar(2);

	Polinomio p1(4);

		p1.cambiar_coeficiente(4,-3);
		p1.cambiar_coeficiente(3,3);
		p1.cambiar_coeficiente(2,-1);
		p1.cambiar_coeficiente(1,6);
		p1.cambiar_coeficiente(0,2);

		p1.print_polinomio();

			Polinomio p_suma = sumar_polinomios(&p, &p1);

			p_suma.print_polinomio();

return 0;
}


void Polinomio::inicializar_coeficientes()
{
	for (int i = 0; i <= m_grado; ++i)
	{
		m_terminos[i] = 0;
	}
}

void Polinomio::cambiar_coeficiente(int coeficiente, int valor)
{
	if (coeficiente <= m_grado+1) m_terminos[m_grado - coeficiente] = valor;
	else cout << "ERROR!" <<"\n";
}

int Polinomio::evaluar(int x)
{
	int result = 0;
	for (int i = 0; i <= m_grado; ++i)
	{
		result += m_terminos[i]*calcular_exponente(x, m_grado-i);
	}
return result;
}

void Polinomio::print_polinomio()
{
cout << endl;

	for (int i = 0; i < m_grado; ++i)
	{
		cout << m_terminos[i] <<"x^"<< m_grado - i;

		if (i != m_grado-1)
		{
			if (m_terminos[i+1] < 0) cout << " ";
			else cout << " + ";
		}
	}
	if (m_terminos[m_grado] > 0) cout << " + " << m_terminos[m_grado];
	else cout << " "<< m_terminos[m_grado];

cout << endl << endl;	
}

int Polinomio::get_grado() const { return m_grado; }

int Polinomio::get_coeficiente(int pos) const { return m_terminos[m_grado - pos]; }


int calcular_exponente(int base, int exponente)
{
if (exponente == 0) return 1;

	return base * calcular_exponente(base,exponente-1);
}

Polinomio sumar_polinomios(Polinomio* p1, Polinomio* p2)
{

int grado_mayor = 0, diferencia = 0;

if (p1->get_grado() > p2->get_grado())
{
	grado_mayor = p1->get_grado();
	diferencia  = grado_mayor - p2->get_grado();

		Polinomio suma(grado_mayor);	

		for (int i = grado_mayor; i > diferencia; --i) suma.cambiar_coeficiente(i, p1->get_coeficiente(i));

		for (int i = diferencia; i <= grado_mayor; ++i)
		{
			suma.cambiar_coeficiente(i, p1->get_coeficiente(p1->get_grado()-i) + p2->get_coeficiente(p2->get_grado()+diferencia-i));
		}

				return suma;
}

else
{ 
	grado_mayor = p2->get_grado();
	diferencia  = grado_mayor - p1->get_grado();

		Polinomio suma(grado_mayor);

		for (int i = grado_mayor; i > diferencia; --i) suma.cambiar_coeficiente(i, p2->get_coeficiente(i));	

		for (int i = grado_mayor-diferencia; i >= 0; --i)
		{
			suma.cambiar_coeficiente(i, p1->get_coeficiente(i) + p2->get_coeficiente(i));
		}

				return suma;
}			
}