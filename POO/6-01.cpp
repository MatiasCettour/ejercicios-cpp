#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <string>
using namespace std;
/*Escriba a través de un programa C++ un archivo binario llamado grupo.dat , ​ formado
por un conjunto de 200 pares de números generados aleatoriamente. Cada par de
datos se conforma por un entero y un flotante. */

struct Par { int entero; float flotante; };

int main(int argc, char const *argv[])
{
	ofstream arch_o("grupo.bin", ios::binary);
	Par aux;

	for (int i = 0; i < 200; ++i)
	{
		aux.entero = rand()%10;
		aux.flotante = rand()%1000;

		arch_o.write(reinterpret_cast<char*>(&aux), sizeof(aux));

		cout << aux.entero << " " << aux.flotante << endl;
	}

return 0;
}