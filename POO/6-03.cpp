#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <string>
using namespace std;
/*Escriba un programa que genere un archivo binario con una lista de 100 enteros
ordenados de forma creciente (por ejemplo: 0, 5, 10, 15, 20, 25...). Luego escriba
otro programa que permita insertar (no reemplazar) un nuevo dato de tipo entero,
ingresado por el usuario, en el archivo manteniendo el orden creciente. Evite utilizar
vectores auxiliares. */

int main(int argc, char const *argv[])
{

	ofstream file_o("numeros.bin", ios::binary);

	int entero = 0;

	cout << "Generando enteros..." << "\n";
	for (int i = 0; i < 100; ++i) 
	{
		entero = i;

		file_o.write(reinterpret_cast<char*>(&entero), sizeof(entero));
	}
file_o.close();

//-------------------------------------------------------------------------------------

	fstream file("numeros.bin", ios::binary | ios::in | ios::out | ios::ate);

	int tam_bytes = file.tellg();
	int n         = tam_bytes/sizeof(entero);

		cout << "Ingrese entero a ingresar > "; cin >> entero;

		int aux = 0;
		int pos = -1;

		file.write(reinterpret_cast<char*>(&aux), sizeof(aux));
		++n;

			file.seekg(0);

			for (int i = 0; i < n; ++i)
			{
				file.read(reinterpret_cast<char*>(&aux), sizeof(aux));

				if (entero <= aux)
				{
					pos = i;
						i = n;
							cout << "Posicion " << pos << " encontrada" << endl;
				}
			}
			if (pos == -1)
			{
				pos = n-1;
				cout << "Guardando en la ultima posicion" << endl;

				file.seekp(pos*sizeof(int));
				file.write(reinterpret_cast<char*>(&entero), sizeof(entero));
			}
			else
			{
				for (int i = n-1; i > pos; --i)
				{
					file.seekg((i-1)*sizeof(int));
					file.read(reinterpret_cast<char*>(&aux), sizeof(aux));

					file.seekp((i)*sizeof(int));
					file.write(reinterpret_cast<char*>(&aux), sizeof(aux));
				}

				file.seekp(pos*sizeof(int));
				file.write(reinterpret_cast<char*>(&entero), sizeof(entero));

				cout << "Datos reorganizados " << endl;

						file.seekg(0);
						for (int i = 0; i < n; ++i)
						{
							file.read(reinterpret_cast<char*>(&aux), sizeof(aux));

							cout << aux << endl;
						}
			}	

return 0;
}
