#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;
/*Desarrolle una clase templatizada llamada ​ ManejadorArchivo que posea métodos
y atributos para manipular un archivo binario que contenga registros del tipo de
dato especificado por el parámetro. La clase debe poseer métodos para:
a. Abrir un archivo binario y cargar los registros en memoria.
b. Obtener el registro en una posición especificada por el usuario.
c. Modificar el registro en una posición determinada.
d. Actualizar la información del archivo con los cambios.
e. Utilice la clase desde un programa cliente para leer los registros escritos en
   el archivo binario generado en el ejercicio 6.5.*/

template<typename T>
class ManejadorArchivo
{
public:
		ManejadorArchivo(string _name)
		{
			file.open(_name, ios::binary|ios::ate|ios::in|ios::out);
			int tam_bytes   = file.tellg();
			n_elementos = tam_bytes/sizeof(T);

				file.seekg(0);
				T aux;
				file.read(reinterpret_cast<char*>(&aux), sizeof(T));

				if (aux == -1) cout << "Archivo no encontrado" << endl;
				else
				{
					file.seekg(0);
					for (int i = 0; i < n_elementos; ++i)
					{
						file.read(reinterpret_cast<char*>(&aux), sizeof(T));
						registro.push_back(aux);
					}
				}
		}

		T get_registro(int i)
		{
			return registro[i];
		}

		int get_size()
		{
			return n_elementos;
		}

		void modificar(int i, T aux)
		{
			if(i <= n_elementos) registro[i] = aux;
		}

		void b_write()
		{
			file.seekg(0);
			for (int i = 0; i < n_elementos; ++i)
			{
				file.write(reinterpret_cast<char*>(&registro[i]), sizeof(T));
			}
		}

private:
	fstream file;
	int n_elementos;
	vector<T> registro;
};

int main(int argc, char const *argv[])
{
	ManejadorArchivo<int> puntajes("tabla1.bin");

	for (int i = 0; i < puntajes.get_size(); ++i)
	{
		cout << puntajes.get_registro(i) << " ";
	}cout << endl;

		puntajes.modificar(0, -1);
		puntajes.b_write();

		for (int i = 0; i < puntajes.get_size(); ++i)
		{
			cout << puntajes.get_registro(i) << " ";
		}cout << endl;

return 0;
}