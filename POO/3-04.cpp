#include <iostream>
#include <string>
using namespace std;

/*a) Proponga una clase ​ RectaGeneral para representar una recta general, cuya
ecuación es ​ Ax+By+C=0 , ​ a partir de dos puntos. El prototipo de la clase se muestra
en el siguiente recuadro.
...
b) Diseñe un árbol de herencia que incluya una clase ​ Recta​ , y dos clases
herederas llamadas ​ RectaExplicita​ y ​ RectaGeneral​ .
c) Utilizando los conceptos de polimorfismo, métodos virtuales y abstractos,
complemente el diseño con dos métodos virtuales: ​ MostrarEcuacion(...) ​ , para
mostrar en pantalla la ecuación que corresponda para cada recta, y ​ Pertenece(...)
para saber si un tercer punto dado está en la recta. ¿Qué problema de diseño
puede marcar respecto al primer método? ​ Nota: al comparar flotantes en el
segundo método, no debe utilizar ==, sino preguntar de alguna otra forma si son
“muy parecidos” en lugar de exactamente iguales ​ .*/

struct Punto { float x = 0, y = 0; };

class Recta
{
public:
	Recta();

	virtual string get_ecuacion() =0;

	virtual~Recta();
};

class Recta_general : public Recta
{
public:
	Recta_general(Punto &p1, Punto &p2);
	string get_ecuacion() override;
	float get_a();
	float get_b();
	float get_c();

private: float a, b ,c;
};

class Recta_explicita : public Recta
{
public:
	Recta_explicita(Punto &p1, Punto &p2);
	string get_ecuacion();
	float print_m(){ cout << "m = " << m; }
	float print_b(){ cout << "b = " << b; }

private: float m, b;
};

Recta_explicita::Recta_explicita(Punto &p1, Punto &p2)
{
	m = (p2.y - p1.y) / (p2.x - p1.x);

	b = p1.y - m*(p1.x);
}

string Recta_explicita::get_ecuacion()
{
	if (b < 0) return "y = " + to_string(m) + "x " + to_string(b);
	else return "y = " + to_string(m) + "x + " + to_string(b);
	
}


int main(int argc, char const *argv[])
{
	Punto p1 = {2,1};
	Punto p2 = {-1, -5};
	Recta_explicita r1(p1,p2);

		cout << r1.get_ecuacion()  <<endl;

	return 0;
}