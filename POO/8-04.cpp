#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <sstream> 
#include <cstring>
#include <iterator>
using namespace std;
/*Declare un arreglo estático de 20 elementos enteros (​ int v[20]; ) ​ y luego:
a. Implemente una función ​ int rand10() que genere un entero aleatorio entre
-10 y 10, y utilícela como argumento para ​ generate para inicializar el arreglo
con valores aleatorios.
b. Implemente una función ​ bool es_par(int x) que retorne true si el entero que
recibe es par; y utilícela en combinación con ​ count_if para contar cuantos
elementos pares hay en el contenedor generado.
c. Implemente una función ​ bool menor_abs(int a, int b) que reciba dos enteros
y retorne verdadero cuando el valor absoluto del primero sea menor que el
valor absoluto del segundo; y utilice esta función en como argumento de
sort ​ para ordenar el vector por valor absoluto.
d. Elimine los elementos repetidos utilizando los algoritmos genéricos de la
STL, y luego muestre el arreglo resultante.*/

int rand10(){ return -10 + rand() % 21;}

bool es_par(int valor) { return !(valor % 2); }

bool menor_abs(int a, int b){ return abs(a) < abs(b); }

int main(int argc, char const *argv[])
{
	int v[20];
	
	auto v_begin = begin(v);
	auto v_end = end(v);
	
	generate(v_begin, v_end, rand10);
	
		for (const int& x : v) cout << x << " ";
	
		size_t cantidad_pares = count_if(v_begin, v_end, es_par);
		
		cout << endl << "Cantidad pares: " << cantidad_pares << endl;
	
			sort(v_begin, v_end, menor_abs);
			
			for (const int& x : v) cout << x << " ";
			cout << endl;
		
				sort(begin(v), end(v));
				auto it_rem 	= unique(begin(v), end(v));
	
				for (auto it = it_rem; it != end(v); ++it)
				{
					*it = -1;
				}
				
				for (const int& x : v) cout << x << " ";
				cout << endl;
	
	return 0;
}
