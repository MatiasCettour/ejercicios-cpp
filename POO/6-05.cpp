#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;
/*Escriba una clase para gestionar la tabla de las N mejores puntuaciones de un
juego. La clase debe permitir ingresar un nuevo puntaje (y encargarse de ubicarlo
en la posición de la tabla que corresponda, o descartarlo), consultar los datos, y
almacenar y recuperar los mismos mediante un archivo binario. */

class Tabla
{
public:

	Tabla(int n) : n_mejores(n) {}

	void add(int puntaje);
	void b_read(string file_name);
	void b_write(string file_name);
	void display_puntajes();
	
private:
	int n_mejores = 0;
	vector<int> puntajes;
};

int main(int argc, char const *argv[])
{
	Tabla t1(10);
	t1.add(5);
	t1.add(6);
	t1.add(7);
	t1.add(2);
	t1.add(9);
	t1.add(10);
	t1.add(11);
	t1.add(13);
	t1.add(5);
	t1.add(5);
	t1.add(7);
	t1.b_write("tabla1.bin");
	t1.display_puntajes();

	Tabla t2(10);
	t2.b_read("tabla1.bin");
	t2.add(15);
	t2.b_write("tabla2.bin");
	t2.display_puntajes();

return 0;
}

void Tabla::add(int puntaje)
{
	if (puntajes.size() == n_mejores)
	{
		if (puntaje < puntajes[0])
		{	
			cout <<" No es digno " << endl;
		}
		else
		{
			puntajes[0] = puntaje;
			sort(puntajes.begin(), puntajes.end());
		}
	}
	else
	{
		puntajes.push_back(puntaje);
		sort(puntajes.begin(), puntajes.end());
	}
}

void Tabla::b_write(string file_name)
{
	ofstream file_o(file_name, ios::binary | ios::trunc);

	for (int& x : puntajes)
	{
		file_o.write(reinterpret_cast<char*>(&x), sizeof(int));
	}

}

void Tabla::b_read(string file_name)
{
	ifstream file_i(file_name, ios::binary | ios::ate);

	int tam_bytes   = file_i.tellg();
	int n_elementos = tam_bytes/sizeof(int);

	int aux;
	file_i.seekg(0);
	for (int i = 0; i < n_elementos; ++i)
	{
		file_i.read(reinterpret_cast<char*>(&aux), sizeof(int));
		puntajes.push_back(aux);
	}
}

void Tabla::display_puntajes()
{
cout << endl;
	for(int& x : puntajes)
	{
		cout << x << " ";
	}
cout << endl;
}