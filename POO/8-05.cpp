#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;
/*Diseñe y programe una clase para manipular datos del tipo que se muestra en el
recuadro.

struct​​ FichaMedico
{
	string nombreMedico;
	vector <​ long​​ > dniPacientes;
};

La clase será utilizada para administrar los pacientes de una clínica. La clínica
está conformada por 6 médicos y cada uno de ellos atiende a un número variable de
pacientes.
Crear una clase llamada ​ Clinica ​ que posea funciones para:
a. Incorporar nuevos pacientes indicando el número de médico y el DNI del
paciente.
b. Listar el nombre de cada médico y los DNI de los pacientes que atiende.
c. Eliminar un paciente indicando solamente su DNI.
Utilice la clase desarrollada desde un programa cliente.*/

struct FichaMedico
{
	string nombreMedico;
	vector<long> dniPacientes;
};


class Clinica
{
public:

	void add_medic(map<int, string>::iterator _it)
	
	{
		vector<long> a;
		m_map_medicos[_it->first] = { _it->second, a };
	}

	void add_paciente(int num_medic, long dni_paciente)
	{
		m_map_medicos[num_medic].dniPacientes.push_back(dni_paciente);
	}
	void listar()
	{
		for (auto it = m_map_medicos.begin(); it != m_map_medicos.end(); ++it)
		{
			cout <<"Medic: "<< it->second.nombreMedico << endl;
			cout <<"Pacientes:" << endl;

			for (auto x : it->second.dniPacientes)
			{
				cout << x << endl;
			}
		cout << endl;
		}
	}
	void remove_paciente(long _dni_paciente)
	{
		for (auto it = m_map_medicos.begin(); it != m_map_medicos.end(); ++it)
		{
			auto it_remove = remove(it->second.dniPacientes.begin(), it->second.dniPacientes.end(), _dni_paciente);
			it->second.dniPacientes.erase(it_remove, it->second.dniPacientes.end());
		}
	}


private:
	map<int, FichaMedico> m_map_medicos;
};

int main(int argc, char const *argv[])
{

map<int, string> medicos;
medicos[1] = "Matias";
medicos[2] = "Alejandro";
medicos[3] = "Lucas";
medicos[4] = "Maria";
medicos[5] = "Elina";
medicos[6] = "Claudia";

	Clinica clinica;

	for (map<int, string>::iterator it = medicos.begin(); it != medicos.end(); ++it)
	{
		clinica.add_medic(it);
	}

	clinica.add_paciente(1, 41268246);
	clinica.add_paciente(1, 41258246);
	clinica.add_paciente(2, 41258246);

	clinica.remove_paciente(41258246);

	clinica.listar();

return 0;
}