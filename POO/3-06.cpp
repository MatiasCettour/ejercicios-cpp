#include <iostream>
using namespace std;
/*Implemente una clase Fraccion para representar una fracción a partir de un
numerador y un denominador, con un método ​ConvertirADouble() para obtener el
real que representa, y los demás métodos que considere necesarios. Implemente
una clase NumeroMixto para representar un número formado por una parte entera
y una fracción impropia (fracción menor a 1). Reutilice la clase ​ Fraccion al
implementar la clase ​ NumeroMixto​. La clase ​ NumeroMixto debe también poseer
un método ​ConvertirADouble(). ¿Qué relación entre clases puede utilizar en este
caso?*/

class Fraccion
{
public:

	void set_numerador  (int valor){ m_numerador = valor; }
	void set_denominador(int valor){ m_denominador = valor; }

	double convertir_a_double(){ return static_cast<double>(m_numerador)/ static_cast<double>(m_denominador); }

	long get_numerador()  { return m_numerador; }
	long get_denominador(){ return m_denominador; }

protected: long m_numerador, m_denominador;
};

class Numero_mixto : public Fraccion
{
public:
	Numero_mixto(int parte_entera, int parte_fraccion) : m_parte_entera(parte_entera)
	{
		m_parte_fraccion.set_numerador(parte_fraccion);

			int digitos = 0;
			do
			{
				++digitos;
				parte_fraccion /= 10;

			} while (parte_fraccion);

			int denominador = 10;
			for (int i = 1; i < digitos; ++i)
			{
				denominador *= 10;
			}

		m_parte_fraccion.set_denominador(denominador);
	}

	double convertir_a_double(){ return static_cast<double>(m_parte_entera) + ( static_cast<double>(m_parte_fraccion.get_numerador()) /
																		  		static_cast<double>(m_parte_fraccion.get_denominador()) ); }
	
protected: 
	int m_parte_entera;
	Fraccion m_parte_fraccion;
};

int main(int argc, char const *argv[])
{
	Numero_mixto a(9,87);

		double n = a.convertir_a_double();

		cout << n;

return 0;
}