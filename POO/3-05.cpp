#include <iostream>
#include <vector>
using namespace std;
/*Implemente una clase ​ Monomio para representar un monomio de la forma a*x​ n a
partir de un coeficiente y un exponente, con un método ​ Evaluar(...) que reciba un
real y retorne el valor del monomio evaluado con ese real, y los demás métodos
que considere necesarios. Implemente, luego, una clase ​ Polinomio que reutilice la
clase ​ Monomio para modelar un polinomio, y añada un método ​ Evaluar(...) para
evaluar un polinomio en un valor x real dado. ¿Qué relación debe haber entre las
clases ​ Monomio​ y ​ Polinomio​ ?*/

int calcular_exponente(int base, int exponente);

class Monomio
{
public:
	Monomio() :  m_coeficiente(0), m_exponente(0){}

	Monomio(int coeficiente, int exponente) : m_coeficiente(coeficiente),
											  m_exponente(exponente){}

	virtual int evaluar(int x){ return m_coeficiente*(calcular_exponente(x, m_exponente)); }

	void cambiar_coeficiente(int valor) { m_coeficiente = valor; }

	void cambiar_exponente(int valor) { m_exponente = valor; }

	int get_coeficiente() const { return m_coeficiente; };

	void display(){ cout << m_coeficiente << "^" << m_exponente <<"\n\n"; }

	virtual~Monomio(){}

protected: int m_coeficiente, m_exponente;
};

class Polinomio : public Monomio
{
public:

	Polinomio(int grado) : Monomio(0,0), m_grado(grado)
	{
		m_miembros = new Monomio[m_grado+1];

		for (int i = 0; i < m_grado; ++i)
		{
			m_miembros[i].cambiar_exponente(i);
		}
	}

	void cambiar_coeficiente(int coeficiente, int valor);

	int evaluar(int x) override;

	void display();

	~Polinomio(){ delete[] m_miembros; };

private:
	int m_grado;
	Monomio* m_miembros = nullptr;
};

int calcular_exponente(int base, int exponente)
{
	if (exponente == 0) return 1;
	else return base*calcular_exponente(base, exponente-1);
}

int Polinomio::evaluar(int x)
{
int suma = 0;
	for (int i = 0; i < m_grado; ++i)
	{
		suma += m_miembros[i].get_coeficiente()*calcular_exponente(x, i);
	}
return suma;
}

void Polinomio::cambiar_coeficiente(int coeficiente, int valor)
{
	if (coeficiente <= m_grado+1) m_miembros[coeficiente].cambiar_coeficiente(valor);
	else cout << "ERROR!" <<"\n";
}

void Polinomio::display()
{
cout << endl;

	for (int i = m_grado; i > 0; --i)
	{
		cout << m_miembros[i].get_coeficiente() << "x^" << i;

		if (m_miembros[i-1].get_coeficiente() > 0) cout << " ";
	}

	if (m_miembros[0].get_coeficiente() > 0) cout << "+ " << m_miembros[0].get_coeficiente();
	else cout << " "<< m_miembros[m_grado].get_coeficiente();

cout << endl << endl;
}

int main(int argc, char const *argv[])
{
	Monomio m1(3,2);
	cout << m1.evaluar(2)<<"\n\n";
	m1.display();

	Polinomio p1(4);
	p1.cambiar_coeficiente(4,1);
	p1.cambiar_coeficiente(3,4);
	p1.cambiar_coeficiente(2,5);
	p1.cambiar_coeficiente(1,2);
	p1.cambiar_coeficiente(0,8);

	p1.display();
	cout << p1.evaluar(2)<<"\n\n";
	return 0;
}