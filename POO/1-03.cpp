#include <iostream>
#include <vector>

using namespace std;
/*A continuación se declara un arreglo a de 10 elementos enteros: int x[10]={110,
120, 130, 140, 150, 160, 170, 180, 190, 200};. El elemento inicial x[0] se ubica en la
dirección de memoria 0x00011E4. Determine lo que que representan las siguientes
expresiones:

a) x;		0x00011E4
c) (x+4);	posicion 4 de memoria
e) ​*x+3; 	contenido primer porcion memoria + 3
b) *x;		contenido primer posicion memoria
d) *(x+4);	contenido posicion 4 
f) ​(*x)+3;  contenido primer porcion memoria + 3*/

int main(int argc, char const *argv[])
{
	int x[10]={110, 120, 130, 140, 150, 160, 170, 180, 190, 200};

	cout << "a) " << x << endl;
	cout << "b) " << *x << endl;
	cout << "c) " << (x+4) << endl;
	cout << "d) " << *(x+4) << endl;
	cout << "e) " << *x+3 << endl;
	cout << "f) " << (*x)+3 << endl;
	return 0;
}
