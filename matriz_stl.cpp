#include <iostream>
#include <cmath>
#include <vector>
#include <tuple>

using namespace std;

int main() { 

	vector < vector <int> > mat;
 // Following statement will "create" a 4x5 vector and fill it with zeroes 
	mat.resize(4, vector<int>(5,0)); 

	for (size_t i = 0; i < mat.size(); i++){ 

		for (size_t j = 0; j < mat[i].size(); j++) cout << mat[i][j] << " ";

		cout << endl;
	} 
} 