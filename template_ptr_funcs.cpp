#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;

struct Alumno
{
	string nombre;
	int edad;
	float prom;
};

template<typename T, typename PFnc>
T menor(const vector<T> &v, PFnc comparador)
{
	T men(v[0]);
	for (size_t i=1; i<v.size(); i++)
	{
		if (comparador(v[i]), men)
		{
			men = v[i];
		}
	}
return men;
}

bool cmp_edad(const Alumno &a1, const Alumno &a2)
{
	return a1.edad < a2.edad;
}

bool cmp_nombre(const Alumno &a1, const Alumno &a2)
{
	return a1.nombre < a2.nombre;
}

bool cmp_promedio(const Alumno &a1, const Alumno &a2)
{
	return a1.prom < a2.prom;
}

int main()
{
	vector<Alumno> v;

	Alumno medad = menor(v,cmp_edad);
	Alumno mnomb = menor(v,cmp_nombre);
	Alumno mprom = menor(v,cmp_prom);
	
return 0;
}



