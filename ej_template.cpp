#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream> 
#include <cstring>
using namespace std;

template<typename T>
class Binario
{
public:
	Binario(string _nombre) : file(_nombre, ios::binary|ios::out|ios::in|ios::trunc)
	{}
	
	int get_n_elements()
	{
		file.seekg(0, ios::end);
		return file.tellg()/sizeof(T);
	}
	T get_value(int i)
	{
		file.seekg(i*sizeof(T));
		T f;
		file.read(reinterpret_cast<char*>(&f), sizeof(T));
	return f;
	}
	void add(T f)
	{
		file.seekg(0, ios::end);
		file.write(reinterpret_cast<char*>(&f), sizeof(T));
	}
	void set(int i, T f)
	{
		file.seekg(i*sizeof(T));
		file.write(reinterpret_cast<char*>(&f), sizeof(T));
	}

private:
	fstream file;
};

int main(int argc, char const *argv[])
{
	Binario<double> b("double.bin");

	b.add(5);
	b.add(6);
	b.add(7);
	b.add(2);
	b.add(9);
	b.add(10);

	int n = b.get_n_elements();
	cout << n << endl;

	for (int i = 0; i < n; ++i)
	{
		cout << b.get_value(i) << " ";
	}

return 0;
}