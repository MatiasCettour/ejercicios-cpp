#include <iostream>

using namespace std;

int cantidad_ciclistas = 0;

struct ciclistas
	{
		string nombre;
		string appellido;

		int minutos_etapa1;
		int segundos_etapa1;
		int minutos_etapa2;
		int segundos_etapa2;
		int minutos_etapa3;
		int segundos_etapa3;
		int suma_min   = 0;
		int suma_seg = 0;

	} ciclista[10000], ganadores[20005];


int main(int argc, char const *argv[])
{
	
	//struct ciclistas ciclista[444];

	cout << "\nIngrese cantidad ciclistas: ";

	do cin >> cantidad_ciclistas;
	while (cantidad_ciclistas <= 1 && cantidad_ciclistas > 10000);

	cout << endl;

	for (auto i = 0; i != cantidad_ciclistas; ++i){

		cout << "\nIngrese nombre competidor nº " << i+1 << ": ";
		cin >> ciclista[i].nombre;

		cout << "Ingrese appellido competidor nº " << i+1 << ": ";
		cin >> ciclista[i].appellido;

		cout << "Ingrese minutos etapa 1 competidor nº " << i+1 << ": ";
		do cin >> ciclista[i].minutos_etapa1;
		while (ciclista[i].minutos_etapa1 < 0);
		ciclista[i].suma_min += ciclista[i].minutos_etapa1;

		cout << "Ingrese segundos etapa 1 (max 60) competidor nº " << i+1 << ": ";
		do cin >> ciclista[i].segundos_etapa1;
		while (ciclista[i].segundos_etapa1 > 60 && ciclista[i].segundos_etapa1 < 0);
		ciclista[i].suma_seg += ciclista[i].segundos_etapa1;


		cout << "Ingrese minutos etapa 2 competidor nº " << i+1 << ": ";
		do cin >> ciclista[i].minutos_etapa2;
		while (ciclista[i].minutos_etapa2 < 0);
		ciclista[i].suma_min += ciclista[i].minutos_etapa2;

		cout << "Ingrese segundos_etapa 2 (max 60) competidor nº " << i+1 << ": ";
		cin >> ciclista[i].segundos_etapa2;
		while (ciclista[i].segundos_etapa2 < 0);
		ciclista[i].suma_seg += ciclista[i].segundos_etapa2;


		cout << "Ingrese minutos etapa 3 competidor nº " << i+1 << ": ";
		do cin >> ciclista[i].minutos_etapa3;
		while (ciclista[i].minutos_etapa3 < 0);
		ciclista[i].suma_min += ciclista[i].minutos_etapa2;

		cout << "Ingrese segundos_etapa 3 (max 60) competidor nº " << i+1 << ": ";
		do cin >> ciclista[i].segundos_etapa3; 
		while (ciclista[i].segundos_etapa3 < 0);
		ciclista[i].suma_seg += ciclista[i].segundos_etapa3;


			while (ciclista[i].suma_seg > 60){

				ciclista[i].suma_seg -= 60;
				ciclista[i].suma_min += 1;
			}

	}

	int menor_min = ciclista[0].suma_min;

	for (auto i = 1; i < cantidad_ciclistas; ++i){

		if (ciclista[i].suma_min < ciclista[i-1].suma_min && ciclista[i].suma_min < menor_min){

																							   menor_min = ciclista[i].suma_min;
																							}

	}

 	int cuenta = 0;
	for (auto x=0; x < cantidad_ciclistas; x++){
												 if (ciclista[x].suma_min == menor_min){
																						ganadores[cuenta] = ciclista[x];
																						++cuenta;
																					} 
	}


	int menor_seg = ganadores[0].suma_seg;

	for (auto i = 1; i < cantidad_ciclistas; ++i){

		if (ganadores[i].suma_seg < ganadores[i-1].suma_seg && ganadores[i].suma_seg < menor_seg){

																							   menor_seg = ganadores[i].suma_seg;
																							}
	}
 
	ganadores[cuenta].suma_min = - 1;
	auto cuenta_segunda 	   = cuenta - 1;
	auto cuenta_tercera 	   = 		  0;
	++cuenta;
	auto indice = cuenta;

	for (auto x=0; x < cuenta_segunda; x++){
												 if (ganadores[x].suma_seg == menor_seg){

																						ganadores[cuenta] = ganadores[x];
																						++cuenta;
																						++cuenta_tercera;
																					} 
	}

	cout << "Menor tiempo: \n";
	for (auto i = indice; i < cuenta_tercera; i++)
				 cout << ganadores[i].nombre << " " << ganadores[i].appellido << "(" << i << ")" << "\n";
	
	return 0;
}