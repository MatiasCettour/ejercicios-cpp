#include<iostream>
#include<vector>
using namespace std;
/*Ejercicio 2 (30 pts) a) Defina un struct Ejercicio para representar un ejercicio de un examen. El struct debe
almacenar un nombre de ejercicio (una palabra que lo identifica), el texto del enunciado, el puntaje que otorga y
el nivel de dificultad (valor real, de 1 a 5). b) Escriba una función, que dado un arreglo de Ejercicios y un
nombre, busque y retorne la posición del ejercicio con ese nombre en el arreglo (nota: los nombres son únicos,
no habrá dos ejercicios con el mismo nombre). Si no hay ninguno con ese nombre, debe retornar -1. c) Escriba
un programa cliente que permita cargar un vector de N ejercicios con todos los ejercicios que alguna vez se
tomaron en la materia (para esta primera carga, N es dato), y luego ingresar nombres de ejercicios para
confeccionar un nuevo examen combinando ejercicios del vector (esta segunda entrada finaliza cuando el
nombre se deja en blanco). Por cada nombre ingresado, debe utilizar la función del apartado b para verificar que
existe y obtener sus datos. Cuando la carga finalice deberá mostrar la dificultad promedio del exámen generado
(promediando las dificultades de los ejercicios seleccionados), y avisar con un mensaje alusivo si los puntajes
seleccionados no suman 100.*/

struct Ejercicio
{
	string nombre, enunciado;
	int puntaje, dificultad;
};

int buscar_ejercicio(vector<Ejercicio> e, string name);

int main(int argc, char const *argv[])
{
vector<Ejercicio> ejercicios;
vector<Ejercicio> examen;

	cout << "Ingrese n ejercicios > ";
	int n; cin >> n;
	ejercicios.resize(n);
	for (size_t i = 0; i < n; ++i)
	{
		cout << "Ingrese nombre ejercicio > ";
		cin >> ejercicios[i].nombre;

		cout << "Ingrese dificultad ejercicio > ";
                cin >> ejercicios[i].dificultad;

		cout << "Ingrese puntaje ejercicio > ";
                cin >> ejercicios[i].puntaje; cin.ignore();

		cout << "Ingrese enunciado ejercicio > ";
		getline (cin,ejercicios[i].enunciado);
	}
			string name = "null";
			int posicion;
			while (!name.empty())
			{
				cout << "Ingrese nombre ejercicio para examen > ";
				getline(cin,name); if(name.empty()) continue;
				posicion = buscar_ejercicio(ejercicios, name);
					if (posicion == -1)
					{	cout << "Ejercicio no encontrado"<<endl;
						continue;
					}
			examen.push_back(ejercicios[posicion]);
			}

int suma1 = 0,suma2 = 0;
	for(Ejercicio x : examen)
	{
	suma1 += x.dificultad;
	suma2 += x.puntaje;
	}
int cantidad_ejercicios = examen.size();
	cout << "Dificultad promedio: " << suma1/cantidad_ejercicios<<endl;

	if (suma2 != 100) cout << "Los ejercicios no suman 100 puntos"<<endl; 
}

int buscar_ejercicio(vector<Ejercicio> e, string name)
{
	for(int i = 0; i < e.size(); ++i) if(e[i].nombre == name) return i;

return -1;
}
