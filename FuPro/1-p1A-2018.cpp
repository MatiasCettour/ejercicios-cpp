//Ejercicio 1 (25 pts) Escriba un programa que permita ingresar primero los nombres de dos equipos defútbol, y luego los 
//resultados de los N partidos que se han disputado entre ambos. El programa deberá mostrar luego el nombre de cada equipo junto 
//con su cantidad de partidos ganados, el porcentaje que dicha   cantidad   representa   sobre   el   total,   y  la   cantidad  
//de   goles   convertidos.   Debe   mostrar   primero  el nombre del equipo que más partidos haya ganado. En caso de empate, 
//primero el nombre del equipo que más goles haya convertido

#include <iostream>
#include <vector>

using namespace std;

int n_partidos = 0;

string equipo1 = "-1";
string equipo2 = "-1";

int partidos_ganados_e1 = 0, partidos_ganados_e2 = 0;

int cantidad_goles_e1 = 0, cantidad_goles_e2 = 0;

float promedio_partidos_ganados_e1 = 0, promedio_partidos_ganados_e2 = 0;

const int maxima_cantidad_partidos = 10;
const int cantidad_equipos 		   = 2;

//                       goles                                   
int resultados [maxima_cantidad_partidos] [cantidad_equipos];

void mostrar_resultados_e1(){

	cout << equipo1 << "\n";
	cout << "partidos ganados: " << partidos_ganados_e1 << "(" << promedio_partidos_ganados_e1 << ")";
	cout << "\ngoles  convertidos" << cantidad_goles_e1;

}

void mostrar_resultados_e2(){

	cout << equipo2 << "\n";
	cout << "partidos ganados: " << partidos_ganados_e2 << "(" << promedio_partidos_ganados_e2 << ")";
	cout << "\ngoles  convertidos" << cantidad_goles_e2;

}
	


int main(int argc, char const *argv[])
{
	cout << "ingrese nombre equipo1: ";
	cin >> equipo1;

	cout << "ingrese nombre equipo2: ";	
	cin >> equipo2;

	cout << "ingrese cantidad de partidos (max. cantidad de partidos: " << maxima_cantidad_partidos << "): ";
	do cin >> n_partidos;
	while (n_partidos > maxima_cantidad_partidos);

		cout << "ingrese los resultados partidos\n\n";

		for (auto i=0; i < n_partidos; i++){

			cout << "ingrese goles partido " << i+1 << " de " << equipo1 << ": ";
			cin >> resultados[i][0];
			cantidad_goles_e1 += resultados[i][0];

			cout << "ingrese goles partido " << i+1 << " de " << equipo2 << ": ";
			cin >> resultados[i][1];
			cantidad_goles_e2 += resultados[i][1];

				if (cantidad_goles_e1 > cantidad_goles_e2) ++partidos_ganados_e1;
				else if (cantidad_goles_e1 < cantidad_goles_e2) ++partidos_ganados_e2;	
		}

		promedio_partidos_ganados_e1 = partidos_ganados_e1 / n_partidos;	
		promedio_partidos_ganados_e2 = partidos_ganados_e2 / n_partidos;

if (partidos_ganados_e1 == partidos_ganados_e2){

	if (cantidad_goles_e1 > cantidad_goles_e2){
		mostrar_resultados_e1();
		mostrar_resultados_e2();
	}
	else if (cantidad_goles_e1 < cantidad_goles_e2){
			mostrar_resultados_e2();
			mostrar_resultados_e1();
	}
}

else if (partidos_ganados_e1 > partidos_ganados_e2){
		mostrar_resultados_e1();
		mostrar_resultados_e2();
}

else if (partidos_ganados_e1 < partidos_ganados_e2){
		mostrar_resultados_e2();
		mostrar_resultados_e1();
}

	return 0;
}