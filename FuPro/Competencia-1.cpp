#include <iostream>
#include <vector>

using namespace std;
/*En una olimpíada estudiantil compiten N alumnos en 3 pruebas (1: matemáticas, 2:
física y 3: computación). Se ingresan por cada inscripto el DNI y su número
asignado para la competencia (entre 1 y N). Luego, sin orden alguno, se van
ingresando ternas con los puntajes de cada prueba: número de participante, código
de prueba, y puntaje en la actividad. Escriba un algoritmo que determine: a) El DNI
del ganador de la competencia y el puntaje total obtenido. b) El DNI del estudiante
que ocupó el 2do lugar y su puntaje. c) ¿Qué puntaje obtuvo en Computación el
ganador de la competencia?*/

struct Competidores
{
	int DNI;
	int codigo;
	vector<int> notas;
};

struct labeled_promedios
{
	float promedio;
	size_t indice;	
};

void agregar_competidor();
void insertar_datos(Competidores &competidor);
void actualizar_competidor();
size_t buscar(const int &code);
size_t buscar_ganador();
size_t buscar_puesto(int puesto);
void ordenar (vector <labeled_promedios> &numeros);

vector<Competidores> competidores;
int main(int argc, char const *argv[])
{
agregar_competidor();
actualizar_competidor();

size_t ganador = buscar_ganador();
size_t segundo = buscar_puesto(2);

cout << "ganador: " << competidores[ganador].DNI << "\n";
cout << "segundo: " << competidores[segundo].DNI << "\n";

}

void agregar_competidor()
{
	Competidores competidor;
	int n = 1;
	char menu = 'y';
	while(menu == 'y')
	{
		cout << "Ingrese DNI: ";
		cin >> competidor.DNI;
		competidor.codigo = n;
		++n;
		competidores.push_back(competidor);
		cout << "agregar otro? y/n ";
		cin >> menu;
	}
}

void actualizar_competidor()
{
Competidores competidor;
competidor.notas.resize(3);
char menu = 'y';

	while(menu == 'y')
	{
	cout << "Ingrese codigo: ";
	cin >> competidor.codigo;
	cout << "Ingrese notas" << endl;
	for (size_t i=0; i<=competidor.notas.size()-1; i++) cin >> competidor.notas[i];

		insertar_datos(competidor);

	cout << "actualizar otro? y/n ";
	cin >> menu;
	}
}

void insertar_datos(Competidores &competidor)
{
	size_t indice = buscar(competidor.codigo);
	competidores[indice].notas = competidor.notas;
}

size_t buscar(const int &code)
{
	for (size_t i = 0; i <= competidores.size()-1; ++i)
	{
		if (competidores[i].codigo == code) return i;
	}
} 

size_t buscar_ganador()
{
	return buscar_puesto(1);
}

size_t buscar_puesto(int puesto)
{
labeled_promedios new_promedio;
vector<labeled_promedios> promedios;

	for (size_t i=0; i<=competidores.size()-1; ++i)
	{
		new_promedio.promedio = (competidores[i].notas[0]+competidores[i].notas[1]+competidores[i].notas[2])/3.;
		new_promedio.indice = i;
			promedios.push_back(new_promedio);
	}
			ordenar(promedios);

				return promedios[puesto-1].indice;
}

void ordenar (vector<labeled_promedios> &numeros)
{

labeled_promedios aux;
bool cambios = true;

	while(1)
	{
	cambios = false;

		for (size_t i=1; i < numeros.size(); i++)
		{
			if (numeros[i-1].promedio < numeros[i].promedio)
			{
				aux = numeros[i-1];
				numeros[i-1] = numeros[i];
				numeros[i]   = aux;

			cambios = true;	
			}
		}
	if (!cambios) break;	
	}
}

