#include <iostream>
#include <ctime>

using namespace std;

int calcular_exponente(int base, int exponente);

int main(int argc, char const *argv[])
{
	
unsigned t0, t1;
 
t0 = clock();

	int a = calcular_exponente(2,2);
	cout << a << endl;

t1 = clock();
 
double time = (double(t1-t0)/CLOCKS_PER_SEC);
cout << "Execution Time: " << time << endl;

	return 0;
}

int calcular_exponente(int base, int exponente){

if (exponente == 0) return 1;

	return base * calcular_exponente(base,exponente-1);
}