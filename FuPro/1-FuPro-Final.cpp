#include<iostream>
#include<vector>
using namespace std;
/*Fundamentos de Programación – Final 08/08/2017
Ejercicio 1 (25pts) Escriba un programa en C++ que simule un sistema de control de acceso por contraseña de
la siguiente manera: a) En una primer etapa debe permitir cargar los datos de los usuarios válidos (pares de
datos: nombre y contraseña). No se conoce la cantidad de usuarios válidos y los datos terminan con el usuario
“ZZZ”. En esta carga inicial, debe verificar mediante un función validar_contraseña que la contraseña no sea
"1234", "asdf", "pass" ni la cadena vacía; mostrando un mensaje de error y solicitando el reingreso de la misma
en estos casos. (nota: proponga usted el prototipo de la función e impleméntela). b) Luego, simular el verdadero
intento de acceso al sistema. El programa debe mostrar el mensaje "Por favor, identifíquese:" y solicitar nombre
y contraseña. Si se ingresa un par nombre+contraseña existente (que coincida con los cargados en el apartado a)
se debe mostrar el mensaje "Bienvenido al sistema sr X" (reemplazando X por el nombre del usuario); sino,
luego de 5 intentos fallidos debe mostrar el mensaje "Fuera bicho!".*/

struct Usuario {string nombre; string password;};

bool check_pass(const string pass);

int main(int argc, char const *argv[])
{
vector<Usuario> usuarios;
Usuario un_usuario;

cout << "ingrese usuarios: " << endl;

for(;;)
{cout << "ingrese usuario: ";
	cin >> un_usuario.nombre; if (un_usuario.nombre == "zzz") break;

	do {cout << "ingrese password: "; cin >> un_usuario.password;}
	while (!check_pass(un_usuario.password));
	usuarios.push_back(un_usuario);
}
	un_usuario.nombre = "";
	un_usuario.password = "";
	cout << "Por favor identifiquese" << endl;

int intentos = 0;
bool acierto = false;
	while(intentos < 5 && !acierto)
	{
		cout << "ingrese usuario: ";
		cin >> un_usuario.nombre;
		cout << "ingrese password: ";
		cin >> un_usuario.password;
		cout << endl;

		for (Usuario x : usuarios)
		{ 
			if (x.nombre == un_usuario.nombre && x.password == un_usuario.password) 
			{	
				acierto = true;
				cout << "Bienvenido al sistema sr " << x.nombre << endl;
		break;
			}
		}
		if (!acierto) ++intentos;
	}
	if (intentos >= 5) cout << "Fuera bicho! " << endl;

return 0;
}

bool check_pass(const string pass)
{
	if(pass == "1234" || pass == "asdf" || pass == "pass" || pass == "") return false;

	else return true;
}