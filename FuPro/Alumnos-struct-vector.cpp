#include <iostream>
//#include <cmath>
#include <vector>
//#include <tuple>

using namespace std;

struct Alumno{
	int DNI;
	vector<float> notas;
};

struct Curso{
	string codigo;
	vector <Alumno> alumnos;
};

void agregar_alumno (vector <Curso> &cursos);

void mostrar_todo   (const vector <Curso> &cursos);

void mostrar_cursos (const vector <Curso> &cursos);

void agregar_curso  (vector <Curso> &cursos);

void mostrar_alumnos(const Alumno &alumno);

int main(){ 

vector <Curso> cursos;

agregar_curso(cursos);
agregar_alumno(cursos);
mostrar_todo(cursos);

}

void mostrar_todo(const vector <Curso> &cursos){

	for (Curso algun_curso : cursos){

		cout << algun_curso.codigo << endl;
		for (size_t i=0; i < algun_curso.alumnos.size(); ++i){

		 mostrar_alumnos(algun_curso.alumnos[i]);
		} 
	}
}

void mostrar_alumnos(const Alumno &alumno){

	cout << "DNI: " << alumno.DNI << endl;
	cout << "Notas: " << endl;
	for (float x : alumno.notas) cout << x << " ";

		cout << endl; 
}

void mostrar_cursos (const vector <Curso> &cursos){

	for (size_t i = 0; i < cursos.size(); ++i)	cout << cursos[i].codigo << endl;
	//for (string &code : cursos.codigo) cout << code << endl;	

}

size_t buscar_curso(string code, const vector <Curso> &cursos){

	for (size_t i = 0; i < cursos.size(); i++)

		if (code == cursos[i].codigo) return i;
}

void agregar_alumno(vector <Curso> &cursos){

	cout << "Ingrese codigo curso: ";
	string code;
	cin >> code;

	size_t indice_curso = buscar_curso(code,cursos);
	size_t tam = cursos[indice_curso].alumnos.size();
	cursos[indice_curso].alumnos.resize(tam+1);
	size_t indice_alumno = tam;

		cout << "Ingrese dni: ";
		cin >> cursos[indice_curso].alumnos[indice_alumno].DNI;

			cout << "Ingrese sus notas:  (-1 para salir)" << endl;
			float nota = 0;

		while (nota != -1){
			cin >> nota;
			if (nota == -1) break;

			cursos[indice_curso].alumnos[indice_alumno].notas.push_back(nota);
	}
}

void agregar_curso(vector <Curso> &cursos){
	
	size_t tam = cursos.size();
	cursos.resize(tam+1);
	size_t indice = tam;

		cout << "Ingrese codigo: ";
		getline(cin,cursos[indice].codigo);
		cin.ignore();

		cout << "se ha agregado el curso " << cursos[indice].codigo << endl;
}