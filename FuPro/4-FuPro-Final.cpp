#include <iostream>
#include <cmath>
#include <vector>
#include <tuple>
#include "print_v.cpp"
/*Libres 15pts) Escriba una función inserta_col que reciba una matriz A de NxM doubles, un vector V de M doubles, y un entero 
 * i entre 0 y M-1. La función debe modificar la matriz A, insertando una nueva columna i- ésima colocando en ella los valores
 *  del vector V.*/
using namespace std;

void inserta_col(vector<vector<double>> &A, vector<double> v, int i);

int main()
{
vector<vector<double>> mat(5,vector<double>(4,0));

for (size_t i = 0; i < mat.size(); ++i)
	for (size_t j = 0; j < mat[i].size(); ++j) mat[i][j] = rand() % 10+10;

print_matriz(mat);

vector<double> array(4);
array = {3,9,6,8};
	inserta_col(mat,array,2);

print_matriz(mat);
}

void inserta_col(vector<vector<double>> &A, vector<double> v, int i)
{
/*A.push_back(vector<double>(4,0));

	size_t j = A.size()-1;

	while(j <= i) 
	{
		A[j] = A[j-1];
	--j;
	}

	A[i] = v;*/

for (size_t k = 0; k < A.size(); ++k) A[k].push_back(0);
	
	size_t j = A[0].size()-1;
	while(j >= i) 
	{
	    for (size_t k = 0; k < A.size(); ++k) A[k][j] = A[k][j-1];
	--j;
	}
	for (size_t k = 0; k < A.size(); ++k) A[k][i] = v[k];
}
