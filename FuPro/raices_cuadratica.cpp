#include <iostream>
#include <cmath>
#include <vector>
#include <tuple>

using namespace std;

struct complex
{
	float real = 0;
	float imag = 0;
};

tuple <complex, complex> calcular_raices_reales(float a ,float b, float c);

tuple <complex,complex> calcular_raices_imaginarias(float a ,float b, float c);

tuple <complex, complex> get_raices(float a ,float b, float c);

float get_discriminante(float a ,float b, float c);

bool cuadratica_real_check(float a ,float b, float c);

void mostrar_valores(complex r1, complex r2);


int main(int argc, char const *argv[])
{ 
	complex raiz1, raiz2;
	float a,b,c;
	cout << "Ingrese a,b y c: ";
	cin >> a >> b >> c;
	tie(raiz1,raiz2) = get_raices(a,b,c);
	mostrar_valores(raiz1,raiz2);

	return 0;
}

tuple <complex, complex> get_raices(float a ,float b, float c){

complex r1,r2;

if (cuadratica_real_check(a,b,c)) tie(r1,r2) = calcular_raices_reales(a,b,c);

else tie(r1,r2) = calcular_raices_imaginarias(a,b,c);

return make_tuple(r1,r2);
}

tuple <complex,complex> calcular_raices_imaginarias(float a ,float b, float c){

	float parte_real, parte_imaginaria;
	float discriminante = get_discriminante(a,b,c);
	discriminante = discriminante * (-1);

	parte_real 		 = -b/(2*a);
	parte_imaginaria = sqrt(discriminante)/(2*a);
	complex r1 = {parte_real,parte_imaginaria};

	parte_imaginaria = (-1)*sqrt(discriminante)/(2*a);
	complex r2 = {parte_real,parte_imaginaria};

return make_tuple(r1,r2);
}

float get_discriminante(float a ,float b, float c){

float discriminante = b*b-4*a*c;
	return discriminante;
 }

tuple <complex, complex> calcular_raices_reales(float a ,float b, float c){

complex r1,r2;
float discriminante = get_discriminante(a,b,c);

	r1.real = (-b + sqrt(discriminante)) / (2.*a);
	r2.real = (-b - sqrt(discriminante)) / (2.*a);

return make_tuple(r1,r2);
}

bool cuadratica_real_check(float a ,float b, float c){

	if (sqrt(get_discriminante(a,b,c)) >= 0) return true;
	else return false;
}

void mostrar_valores(complex r1, complex r2){

	if (r1.imag == 0 && r2.imag == 0) cout << "x1: " << r1.real << "\n" << "x2: " << r2.real << endl;

	else cout << "x1: " << r1.real << " + " << r1.imag <<"i"<<"\n" << "x2: " << r2.real <<" + "<<r2.imag<<"i"<< endl;
}