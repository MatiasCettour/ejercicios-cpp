/*Un struct definido como struct Evento { string tipo; int jugador; }; representa un evento
ocurrido en un partido de tenis. tipo puede ser “ace”, “error no forzado”, “tiro ganador”, “doble falta”, y
”quiebre”. jugador puede ser 1 o 2. a) Escriba una función llamada resumen_partido que reciba un
arreglo/vector de structs de tipo Evento y un número de jugador, y retorne la cantidad de eventos de cada tipo
para ese jugador. b) Escriba un programa cliente que permita ingresar todos los eventos de un partido durante el
mismo (sin conocer previamente la cantidad de eventos), y luego muestre en pantalla el resumen de cada
jugador.*/

#include <iostream>
#include <vector>

using namespace std;

struct evento { string tipo; int jugador; };

int resumen_partido (const vector<evento> &datos, int num_jugador);


int main(int argc, char const *argv[])
{
vector<evento> datos;

char menu = 'y';
evento algun_dato;

	while(menu == 'y'){	
		
		cout << "ingrese jugador 1 o 2: ";
		cin >> algun_dato.jugador;
		cin.ignore();

		cout << "ingrese evento: ";
		do getline(cin, algun_dato.tipo);
		while (algun_dato.tipo != "ace" && algun_dato.tipo != "error no forzado" && algun_dato.tipo != "tiro ganador" && algun_dato.tipo != "doble falta");

		datos.push_back(algun_dato);

	cout << "desea ingrear otro evento? y/n: ";

	do cin >> menu;
	while (menu != 'y' && menu != 'n');
cout << endl;
	}
///////////////////////////////////////
cout << "jugador1 " <<"("<<resumen_partido(datos,1)<<")"<<"\n";
		for (evento x : datos)
		{
			if (x.jugador == 1) cout << x.tipo << "\n";
		}cout << endl;

cout << "jugador2 " <<"("<<resumen_partido(datos,2)<<")"<<"\n";
		for (evento x  : datos)
		{
			if (x.jugador == 2) cout << x.tipo << "\n";
		}cout << endl;

	return 0;
}


int resumen_partido(const vector<evento> &datos, int num_jugador)
{
int cuenta = 0;

	for (evento x : datos)
	{
		if (x.jugador == num_jugador) ++cuenta;
	}

	return cuenta;
}