#include <iostream>
#include <vector>

using namespace std;
/*
Ejercicio 9.10

Se leen los datos de una matriz o tabla de 5x12 que contiene los valores en mm de
lluvias producidas en 5 departamentos de la provincia durante los 12 meses de
2005. Se sabe que en algunos departamentos tienen datos faltantes por lo que
aparecen valores negativos (-1) en cada uno de esos casos. Escriba un programaC++ 
que: a) complete los datos de lluvia faltantes con el promedio anual de
precipitación en el departamento correspondiente. Cada promedio debe obtenerse
con los datos reales existentes (no contar los -1). b) Amplíe la matriz agregando
una nueva columna con los totales de mm caídos en todo el año en cada
departamento. Informe: 1) la matriz original de datos, 2) la matriz modificada y 3)
los totales anuales de lluvia por departamento.
*/
vector<vector<int>> completar_datos(const vector <vector<int>> &matriz);
void agregar_promedio(vector <vector<int>> &matriz);

const int filas    = 5;
const int columnas = 12;

int main(int argc, char const *argv[])
{
	vector<vector<int>> matriz(filas, vector<int>(columnas,0));

		for (size_t i = 0; i < matriz.size(); ++i)
		{
			for (size_t j = 0; j < matriz[i].size(); ++j)
			{
			 	matriz[i][j] = random() %12-1;
			 	cout << matriz[i][j] << " ";
			}	

			cout << endl;	
		}
cout << endl;

	vector<vector<int>> matriz_2 = completar_datos(matriz);
	agregar_promedio(matriz_2);

	for (size_t i = 0; i < matriz_2.size(); ++i)
		{
			for (size_t j = 0; j < matriz_2[i].size(); ++j) cout << matriz_2[i][j] << " ";
			cout << endl;	
		}

	return 0;
}

vector<vector<int>> completar_datos(const vector <vector<int>> &matriz)
{
vector <vector<int>> matriz_2 = matriz;
int suma, promedio;
bool menos_uno = false;
vector <size_t> menos_unos;

	for (size_t i = 0; i < matriz_2.size(); ++i)
	{
	menos_uno = false;
	menos_unos.clear();

		for (size_t j = 0; j < matriz_2[i].size(); ++j)
		{
		 	suma += matriz_2[i][j];

			 	if (matriz_2[i][j] == -1)
			 	{
			 	 	menos_uno = true;
			 	 	menos_unos.push_back(j);
			 	}
		}
		if (menos_uno)
		{
			promedio = suma/matriz_2[i].size();
			for (size_t x : menos_unos) matriz_2[i][x] = promedio;
		}		
	}
return matriz_2;
}

void agregar_promedio(vector <vector<int>> &matriz)
{
int suma = 0;
		for (size_t i = 0; i < matriz.size(); ++i)
		{
			for (size_t j = 0; j < matriz[i].size()-1; ++j) suma += matriz[i][j];
			matriz[i].push_back(suma/columnas);
			suma = 0;
		}
}
