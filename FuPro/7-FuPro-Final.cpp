#include<iostream>
#include<vector>
#include"print_v.cpp"
/*Ejercicio 3 (25 pts) Escriba una función que reciba una matriz de flotantes, y dos números de filas (f1 y f2), y
reemplace todas las filas desde f1 hasta f2 (incluyendo ambas) por una única fila conformada en cada columna
por el promedio de los datos eliminados de esa columna.*/

const int filas    = 5;
const int columnas = 4;

void reemplace(vector<vector<float>> &mat,int f1, int f2);

int main(int argc, char const *argv[])
{
vector<vector<float>> matriz(filas,vector<float>(columnas));
for (size_t i = 0; i < filas; i++)
	for (size_t j = 0; j < columnas; j++)
			matriz[i][j] = rand()%20+1;
print_matriz(matriz);

reemplace(matriz,1,2);
print_matriz(matriz);
}

void reemplace(vector<vector<float>> &mat,int f1, int f2)
{
int n = 0;
for(int i = f1; i <= f2; ++i) ++n;
float suma = 0;
vector<float> promedios;

	for (int j = 0; j < columnas; ++j)
	{
		suma = 0;
		for(int i = f1; i <= f2; ++i) suma += mat[i][j];

	promedios.push_back(suma/n);
	}
		for(int j = 0; j < columnas; ++j) mat[f1][j] = promedios[j];

			mat.erase(mat.begin()+f1+1,mat.begin()+f2+1);
}