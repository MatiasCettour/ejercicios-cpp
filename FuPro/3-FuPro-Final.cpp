#include <iostream>
#include <cmath>
#include <vector>
#include <tuple>
/*a) Defina un struct Licencia para representar una licencia de un software. El struct debe tener
campos para guardar el nombre del software (ejemplo: “ZinjaI”), el nombre de licencia (ejemplo “GPL”) y el
costo de la misma en dólares (ej: 0). b) Implemente una función calcular_costos que reciba dos vectores, uno con
una lista de licencias, y otro de igual tamaño con una lista de enteros, que indican las cantidades de cada una de
las licencias que necesita adquirir cierta institución. La función debe calcular y retornar el costo total de la
compra de todas esas licencias. c) Implemente un programa cliente que permita cargar los datos de N programas
(por cada uno, su licencia y la cantidad a comprar) y calcule y muestre el costo total.*/
using namespace std;

struct Licencia {

	string nombre_sofware;
	string nombre_licencia;
	string costo;
}

int main(int argc, char const *argv[])
{
vector<Licencia> lista_licencias (5);
vector<int>      lista_enteros   (5);

	for (size_t i = 0; i < lista_licencias.size(); ++i)
	{
		cin >> lista_licencias.nombre_sofware;
		cin >> lista_licencias.nombre_licencia;
		cin >> lista_licencias.costo;
	}

	for (int &x : lista_enteros) x = rand() % 10+1;

return 0;
}


int calcular_costos(vector<Licencia> lista_lic, vector<int> lista_ent)
{
	int costo = 0;
	for (size_t i = 0; i < lista_licencias.size(); ++i)
	{
		costo += lista_lic[i].costo * lista_ent[i];
	}
return costo;
}