#include <iostream>
#include <vector>

using namespace std;

void print_matriz(const vector<vector<int>> &matriz)
{
	for (size_t i = 0; i < matriz.size(); ++i)
	{
		for (size_t j = 0; j < matriz[i].size(); ++j) cout << matriz[i][j] << " "; 
		cout << endl;
	}
	cout << endl;
}

void print_matriz(const vector<vector<double>> &matriz)
{
	for (size_t i = 0; i < matriz.size(); ++i)
	{
		for (size_t j = 0; j < matriz[i].size(); ++j) cout << matriz[i][j] << " "; 
		cout << endl;
	}
	cout << endl;
}

void print_matriz(const vector<vector<float>> &matriz)
{
	for (size_t i = 0; i < matriz.size(); ++i)
	{
		for (size_t j = 0; j < matriz[i].size(); ++j) cout << matriz[i][j] << " "; 
		cout << endl;
	}
	cout << endl;
}

void print_vector(const vector<int> v)
{
	for (int x : v) cout << x << " ";
	cout << endl;
}

void print_vector(const vector<double> v)
{
	for (double x : v) cout << x << " ";
	cout << endl;
}

void print_vector(const vector<float> v)
{
	for (float x : v) cout << x << " ";
	cout << endl;
}

