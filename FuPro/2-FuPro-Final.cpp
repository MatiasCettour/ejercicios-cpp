#include <iostream>
#include <cmath>
#include <vector>
#include <tuple>
/*La Fórmula 1 está buscando mejorar la seguridad agregando nuevos elementos para proteger
las cabezas de los pilotos. Se probaron tres sistemas: el "shield" (una especie de "parabrisas" blindado), el "halo"
(un armazón de carbono) y el "ghost" (sin piloto en el auto, manejado con control remoto). Ciertos equipos han
probado algunos de estos elementos y han calificado los resultados en una escala de 1 a 5 estrellas. Se requiere
un programa C++ para analizar estos datos y ayudar a decidir cuál es la mejor opción. a) Se ingresan ternas de
datos compuestas por un código de equipo (de 1 a 10), un código de sistema (1-shield, 2-halo, 3-ghost), y la
calificación (1 a 5 estrellas). Los datos finalizan con código de equipo 0. Organice estos datos en una matriz de
10x3 (10 equipos, 3 sistemas), donde cada posición debe guardar la cantidad de estrellas asignadas por ese
equipo a ese sistema, o -1 si ese equipo no probó ese sistema. b) muestre solo los equipos que calificaron los tres
sistemas, con sus respectivas calificaciones. c) Informe el nombre del sistema que obtuvo un mayor promedio de
estrellas (el promedio se calcula considerando todos los equipos que probaron el sistema, sin considerar los -1).
Defina y utilice para ello una función calcular_promedio que calcule el promedio de un sistema.*/
using namespace std;

void print_matriz    (const vector<vector<int>> &matriz);
void mostrar_equipos (const vector<vector<int>> &mat);
vector<vector<int>> get_matriz_organizada (const vector<vector<int>> &matriz);
float calcular_promedio(const vector<vector<int>> &mat_sist, int sist);
int get_mejor_sistema(const vector<vector<int>> &matriz_o);

int main(int argc, char const *argv[])
{
vector<vector<int>> matriz(30,vector<int>(3));
vector<int> 		terna (3);

	for (size_t i = 0; i < matriz.size(); ++i)
	{
		cout << "Ingrese codigo equipo (1 a 10): ";//terna[0] = rand() % 10+1;cout << endl;
		cin >> terna[0]; cout << endl;

		cout << "Ingrese codigo de sistema: ";//terna[1] = rand() % 3+1;cout << endl;
		cin >> terna[1]; cout << endl;

		cout << "Ingrese calificacion: ";//terna[2] = rand() % 5+1;cout << endl;
		cin >> terna[2]; cout << endl;

			matriz[i] = terna;
	}							print_matriz(matriz); cout << endl;

vector<vector<int>> matriz_ordenada = get_matriz_organizada(matriz);

	print_matriz(matriz_ordenada);
}

void print_matriz(const vector<vector<int>> &matriz)
{
	for (size_t i = 0; i < matriz.size(); ++i)
	{
		for (size_t j = 0; j < matriz[i].size(); ++j) cout << matriz[i][j] << " "; 
		cout << endl;
	}
}

vector<vector<int>> get_matriz_organizada (const vector<vector<int>> &matriz)
{
vector<vector<int>> mat_o(10,vector<int>(3));

	for (size_t i = 0; i < matriz.size(); ++i)
	{
		mat_o [ matriz[i][0]-1 ][ matriz[i][1]-1 ] = matriz[i][2];
	}
return mat_o;	
}

void mostrar_equipos (const vector<vector<int>> &matriz_o)
{
	for (size_t i = 0; i < matriz_o.size(); ++i)
	{
		if (matriz_o[i][0] == -1 || matriz_o[i][1] == -1 || matriz_o[i][2] == -1) continue;

		cout << "Equipo "<< i+1 << "\n";
		for (size_t j = 0; i < matriz_o[i].size(); ++j)
		{
			cout << matriz_o[i][j] << " ";
		}
	}

}

float calcular_promedio(const vector<vector<int>> &mat_sist, int sist)
{
	int suma = 0;
	for (size_t i = 0; i < mat_sist.size(); ++i)
	{
		if (mat_sist[i][sist] == -1) continue;

	 	suma += mat_sist[i][sist];
	}
		return suma/10.;
}

int get_mejor_sistema(const vector<vector<int>> &matriz_o)
{
float promedio1 = calcular_promedio (matriz_o,0);
float promedio2 = calcular_promedio (matriz_o,1);
float promedio3 = calcular_promedio (matriz_o,2);

	if (promedio1 > promedio2 && promedio1 > promedio3) return 0;

	if (promedio2 > promedio1 && promedio2 > promedio3) return 1;

	if (promedio3 > promedio1 && promedio3 > promedio2) return 2;

}
