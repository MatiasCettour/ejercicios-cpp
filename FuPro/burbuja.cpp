#include <iostream>
#include <vector>

using namespace std;

void ordenar (vector <int> &numeros);


int main(int argc, char const *argv[])
{

vector <int> numeros (20);
	
for (size_t i = 0; i<numeros.size(); ++i) numeros[i] = random()% 100+1;
for (int &x : numeros) cout << x << endl;	
cout << "********************";
cout << endl;
ordenar(numeros);

for (int &x : numeros) cout << x << endl;

}

void ordenar (vector <int> &numeros)
{

int aux;
bool cambios = true;

	while(1)
	{
	cambios = false;

		for (size_t i=1; i<numeros.size(); i++)
		{
			if (numeros[i-1] > numeros[i])
			{
				aux = numeros[i-1];
				numeros[i-1] = numeros[i];
				numeros[i]   = aux;

			cambios = true;	
			}
		}
	if (!cambios) break;	
	}
}
